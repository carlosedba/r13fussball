<?php
namespace Rise\Models;

use Rise\Model;

class Component extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_components';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'alias',
    ];
}
?>