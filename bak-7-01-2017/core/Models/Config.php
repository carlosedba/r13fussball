<?php
namespace Rise\Models;

use Rise\Model;

class Config extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_config';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'value', 'autoload',
    ];
}
?>