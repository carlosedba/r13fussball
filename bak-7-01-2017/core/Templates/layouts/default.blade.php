<!DOCTYPE html>
<html lang="pt-BR">
	@include('includes.head')
	<body>
		@section('content')
			@show
		@include('includes.footer')
		@include('includes.scripts')
	</body>
</html>