<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	@section('css')
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/vendor/normalize/normalize.css">
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/vendor/hamburgers/hamburgers.min.css">
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/vendor/flickity/flickity.min.css">
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/vendor/heeSlider/hee-slider.css">
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/resources/css/colors.css">
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/resources/css/buttons.css">
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/resources/css/inputs.css">
		<link rel="stylesheet" type="text/css" href="{{ $theme }}/resources/css/main.css">
  		<link rel="stylesheet" type="text/css" href="{{ $theme }}/static/css/app.26ef044608b2551213a88a28fac58c11.css"/>
  		<!-- Fonts -->
  		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
  		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
		@show
	@section('js')
		<script src="{{ $theme }}/vendor/modernizr-2.8.3.min.js"></script>
		<script src="{{ $theme }}/vendor/bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
		@show
</head>