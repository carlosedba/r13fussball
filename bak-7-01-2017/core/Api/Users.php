<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class Users
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$users = Model::factory('User')->findArray();
		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$users = Model::factory('User')->where('id', $id)->findOne()->asArray();
		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOne(Request $request, Response $response, $args)
	{
		$email = $args['email'];
		$users = Model::factory('User')->where('email', $email)->findOne()->asArray();
		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function limit(Request $request, Response $response, $args)
	{
		$params = $request->getParams();
		$start = $params['start'];
		$limit = $params['limit'];

		$users = Model::factory('User')->limit($limit)->offset($start)->findArray();
		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$user = Model::factory('User')->create(array(
			'id' 			=> IdGenerator::uniqueId(8),
			'first_name' 	=> $data['first_name'],
			'last_name' 	=> $data['last_name'],
			'email' 		=> $data['email'],
			'password' 		=> hash('sha256', $data['password']),
			'picture' 		=> $data['picture'],
		));

		if ($user->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();

		if (!empty($files['picture'])) {
			$picture = Upload::saveTo(CONTENT_DATA_PATH, $files['picture']);
			$data['picture'] = $picture;
		}

		$user = Model::factory('User')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($user->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$user = Model::factory('User')->where('id', $id)->findOne();

		if ($user->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
