<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Upload;
use Rise\Utils\IdGenerator;

class Content
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$params = $request->getQueryParams();
		$component = $params['id_component'];

		if (isset($component) && !empty($component)) {
			$contents = Model::factory('Content')
						->where('id_component', $component)
						->findArray();
		} else {
			$contents = Model::factory('Content')->findArray();
		}

		$json = json_encode($contents);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$contents = Model::factory('Content')->where('id', $id)->findOne()->asArray();
		$json = json_encode($contents);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}
	
	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$picture = Upload::saveTo(CONTENT_DATA_PATH, $files['picture']);

		$content = Model::factory('Content')->create(array(
			'id' 			=> IdGenerator::uniqueId(8),
			'id_component' 	=> $data['id_component'],
			'title' 		=> $data['title'],
			'description' 	=> $data['description'],
			'category' 		=> $data['category'],
			'content' 		=> $data['content'],
			'url' 			=> $data['url'],
			'picture' 		=> $picture,
			'order' 		=> $data['order'],
			'status' 		=> $data['status'],
		));

		if ($content->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();

		if (!empty($files['picture'])) {
			$picture = Upload::saveTo(CONTENT_DATA_PATH, $files['picture']);
			$data['picture'] = $picture;
		}

		$content = Model::factory('Content')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($content->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$content = Model::factory('Content')->where('id', $id)->findOne();

		if ($content->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
