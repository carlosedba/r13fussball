<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class Components
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$params = $request->getQueryParams();
		$type = $params['type'];

		if (isset($type) && !empty($type)) {
			$components = Model::factory('Component')
						->where('type', $type)
						->findArray();
		} else {
			$components = Model::factory('Component')->findArray();
		}

		$json = json_encode($components);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$components = Model::factory('Component')->where('id', $id)->findOne()->asArray();
		$json = json_encode($components);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}
	
	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$component = Model::factory('Component')->create(array(
			'name' 			=> $data['name'],
			'alias' 		=> $data['alias'],
		));

		if ($component->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$component = Model::factory('Component')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($component->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$component = Model::factory('Component')->where('id', $id)->findOne();

		if ($component->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
