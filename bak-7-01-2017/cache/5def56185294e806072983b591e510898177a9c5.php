<?php $__env->startSection('title', $config['site_name']); ?>
<?php $__env->startSection('description', $config['site_description']); ?>
<?php $__env->startSection('css'); ?>
	##parent-placeholder-2f84417a9e73cead4d5c99e05daff2a534b30132##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##
		<!-- Rise Application -->
		<div class="rise"></div>

		<!-- Website -->
		<div class="site">		
			<header class="header" data-editable="true" data-component="menu" data-id="1">
				<!-- Navigation Bar -->
				<nav class="navbar">
					<div class="navbar-left">
						<div class="navbar-logo">
							<a href="<?php echo e($config['site_home']); ?>">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" viewBox="0 0 740.9 168.9" xml:space="preserve"><style type="text/css">.st0{clip-path:url(#SVGID_2_);fill:#CD2F44;} .st1{clip-path:url(#SVGID_4_);fill:url(#SVGID_5_);} .st2{clip-path:url(#SVGID_7_);fill:#CD2F44;} .st3{clip-path:url(#SVGID_9_);fill:url(#SVGID_10_);} .st4{clip-path:url(#SVGID_12_);fill:url(#SVGID_13_);} .st5{clip-path:url(#SVGID_15_);fill:url(#SVGID_16_);} .st6{clip-path:url(#SVGID_18_);fill:url(#SVGID_19_);} .st7{clip-path:url(#SVGID_21_);fill:url(#SVGID_22_);} .st8{clip-path:url(#SVGID_24_);fill:url(#SVGID_25_);} .st9{clip-path:url(#SVGID_27_);fill:url(#SVGID_28_);}</style><defs><rect id="SVGID_1_" width="740.9" height="168.9"/></defs><clipPath id="SVGID_2_"><use xlink:href="#SVGID_1_" overflow="visible"/></clipPath><path class="st0" d="M72,26.8C72,9.5,63.1,0.9,45.2,0.9H0c-0.3,17.2,5.5,26,17.5,26.3l17.1,0.5c7.1,0,10.6,3.7,10.6,11.1v130.1 c17.7,0,26.6-7.2,26.8-21.5V26.8z"/><defs><path id="SVGID_3_" d="M72.9,0c-0.6,17.2,7.7,25.8,24.9,25.8h51.1c17.2-0.3,26.6,6.5,28.2,20.3c-2.8,13.8-12.2,20.9-28.2,21.2 H97.9c0,17.8,8.8,26.8,26.3,26.8h24.8c17.5,0,26.3,8.1,26.3,24.2c0,0.4-0.1,0.8-0.1,1.2l0.1-0.1l0,0v13.2c0,0-1.7,36.2,30,36.2 h10.3c0,0-10.3-5.7-10.3-14.5v-35.3c-0.3-26-5.7-38.9-16.2-38.8c10.5-4,15.7-15.5,15.7-34.6c0.3-30.3-18.1-45.6-55.3-45.7H72.9z"/></defs><clipPath id="SVGID_4_"><use xlink:href="#SVGID_3_" overflow="visible"/></clipPath><radialGradient id="SVGID_5_" cx="0" cy="168.9" r="1" gradientTransform="matrix(78.1645 0 0 -78.1645 144.222 13287.296)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="72.3" y="0" class="st1" width="143.2" height="168.9"/><defs><rect id="SVGID_6_" width="740.9" height="168.9"/></defs><clipPath id="SVGID_7_"><use xlink:href="#SVGID_6_" overflow="visible"/></clipPath><path class="st2" d="M148.9,142.6H97.9c-17.2,0.3-25.8,9.1-25.8,26.3h76.9c12.9,0,23.5-2,32-5.9c-8.7-8.3-9.4-20.2-9.6-29.7 C167.2,139.6,160,142.4,148.9,142.6"/><g><defs><path id="SVGID_8_" d="M263,63.9v56.8H277V79h1.9c3.8,0,6.9-3.2,6.9-7v-3.2h-9v-3.7c0.3-2.7,2.1-4.1,5.3-4.1h3.7V49.5 c-2.1-0.3-4.3-0.4-6.7-0.4C268.7,49.1,262.5,53.3,263,63.9"/></defs><clipPath id="SVGID_9_"><use xlink:href="#SVGID_8_" overflow="visible"/></clipPath><radialGradient id="SVGID_10_" cx="0" cy="168.9" r="1" gradientTransform="matrix(26.541 0 0 -26.541 274.374 4567.936)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="262.5" y="49.1" class="st3" width="23.2" height="71.5"/></g><g><defs><path id="SVGID_11_" d="M363.5,65.3c-4.6,0-6.8,2.3-6.7,6.9v29.2c0,6.2-3.1,9.2-9.4,9.2c-6.3,0-9.4-3-9.4-9.2V65.3H324v33.3 c0,9.3,2.4,15.4,7.2,18.4c4.8,3.1,9.9,4.7,15.7,4.7c6,0,11.4-1.5,16.3-4.3c4.9-3,7.3-9.3,7.3-18.8V65.3H363.5z"/></defs><clipPath id="SVGID_12_"><use xlink:href="#SVGID_11_" overflow="visible"/></clipPath><radialGradient id="SVGID_13_" cx="0" cy="168.9" r="1" gradientTransform="matrix(25.8516 0 0 -25.8516 347.31 4460.14)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="324" y="65.3" class="st4" width="46.6" height="56.4"/></g><g><defs><path id="SVGID_14_" d="M510.7,92.8c0.4-10.7,4.3-15.9,11.8-15.9c7.5,0,11.4,5.2,11.7,15.9c0,11.2-4,16.8-11.9,16.8 C514.3,109.7,510.6,104,510.7,92.8 M496.8,49.5v47.2c0,16.6,8.7,24.9,25.9,24.8c17.2-0.3,25.7-9.6,25.7-28 c0-18.8-7.3-28.5-22.1-29c-6.9,0-12.1,2.8-15.7,8.6V56.5c0-4.6-2.3-7-6.9-7H496.8z"/></defs><clipPath id="SVGID_15_"><use xlink:href="#SVGID_14_" overflow="visible"/></clipPath><radialGradient id="SVGID_16_" cx="0" cy="168.9" r="1" gradientTransform="matrix(31.3227 0 0 -31.3227 522.612 5376.27)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="496.8" y="49.5" class="st5" width="51.7" height="72.1"/></g><g><defs><path id="SVGID_17_" d="M598.6,103.9c0-4.1,2.8-6.7,8.4-7.7c4.1-0.8,7.8-2,11.1-3.4V98c0,7.8-4.7,12.4-12.4,12.4 C601.2,110.4,598.9,108.3,598.6,103.9 M586.6,79.8h13.9c0.7-2.6,3.4-3.9,8.4-3.9c5.8,0,8.7,1.6,9,4.7c0,3-3,4.8-9,5.5 c-14,1.4-23.6,4.9-24,18.2c0,12.2,9.2,18.4,22.5,17.4c14.9-1,24.2-7.9,24.2-23.1V79.8c-0.3-10.2-8.1-15.3-23.3-15.3 C595.4,64.5,588.2,69.5,586.6,79.8"/></defs><clipPath id="SVGID_18_"><use xlink:href="#SVGID_17_" overflow="visible"/></clipPath><radialGradient id="SVGID_19_" cx="0" cy="168.9" r="1" gradientTransform="matrix(26.1307 0 0 -26.1307 608.202 4506.913)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="584.9" y="64.5" class="st6" width="46.6" height="58.3"/></g><g><defs><path id="SVGID_20_" d="M671.7,49.5v71.1h13.9V56.3c0-3.7-3-6.8-6.7-6.8H671.7z"/></defs><clipPath id="SVGID_21_"><use xlink:href="#SVGID_20_" overflow="visible"/></clipPath><radialGradient id="SVGID_22_" cx="0" cy="168.9" r="1" gradientTransform="matrix(25.6222 0 0 -25.6222 678.665 4412.964)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="671.7" y="49.5" class="st7" width="13.9" height="71.1"/></g><g><defs><path id="SVGID_23_" d="M727,49.5v71.1h13.9V56.3c0-3.7-3-6.8-6.7-6.8H727z"/></defs><clipPath id="SVGID_24_"><use xlink:href="#SVGID_23_" overflow="visible"/></clipPath><radialGradient id="SVGID_25_" cx="0" cy="168.9" r="1" gradientTransform="matrix(25.6222 0 0 -25.6222 733.905 4412.967)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="727" y="49.5" class="st8" width="13.9" height="71.1"/></g><g><defs><path id="SVGID_26_" d="M417.1,52.1c-6.9,6.9-7.1,16.8-7.1,17.9V111c0,9.4,8.9,10.5,8.9,10.5h3.9V70.1c0-2.6,0.8-11.2,9.9-11.2 c8.9,0,9,6.4,9,6.4c0,3.2-1,4.1-2.9,6c-2.1,2.1-5.3,5.2-5.3,10.7c0,9.2,7.5,12.1,12,13.9c3.4,1.3,5.1,2.1,5.6,3.6 c0.9,2.6,0.7,5.2-0.6,7.2c-1.5,2.2-4.5,3.4-8.4,3.4c-1.4,0-2.7-0.7-3.9-1.6c-0.1,0-0.1,0-0.2-0.1c-4.1-2.2-8.2-0.9-10.9,0.5 l-3.8,2.4c0.8,1.2,7.8,11.6,18.8,11.6c8.2,0,14.9-3.1,18.9-8.8c3.7-5.3,4.6-12.3,2.2-18.9c-2.6-7.1-8.9-9.6-13.1-11.2 c-1.4-0.5-3.4-1.3-3.8-1.9c0.1-0.3,0.9-1,1.5-1.6c2.5-2.5,6.7-6.6,6.7-15.1c0-7.4-5.9-19.3-21.8-19.3 C426.5,46,421.1,48.1,417.1,52.1"/></defs><clipPath id="SVGID_27_"><use xlink:href="#SVGID_26_" overflow="visible"/></clipPath><radialGradient id="SVGID_28_" cx="0" cy="168.9" r="1" gradientTransform="matrix(33.3147 0 0 -33.3147 437.373 5711.68)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#304382"/><stop offset=".2" stop-color="#304382"/><stop offset="1" stop-color="#283F63"/></radialGradient><rect x="410.1" y="46" class="st9" width="55.6" height="76.8"/></g></svg>
							</a>
						</div>
					</div>
					<div class="navbar-center">
						<ul class="navbar-menu">
							<li><a href="#">Início</a></li>
							<li><a href="#">A Empresa</a></li>
							<li><a href="#">Serviços</a></li>
							<li><a href="#">Portfólio</a></li>
							<li><a href="#">Clientes/Parceiros</a></li>
							<li><a href="#">Contato</a></li>
						</ul>
					</div>
					<div class="navbar-right">
						<div class="navbar-search hidden">
							<input type="text" name="elemento"/>
							<button type="submit">
								<div class="navbar-search-icon">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 409.6 409.6" xml:space="preserve"><style type="text/css">.ab1{fill:#C03443;}</style><path class="ab1" d="M398.4 344l-94-94c-14 21.8-32.5 40.3-54.3 54.3l94 94c15 15 39.3 15 54.3 0C413.4 383.4 413.4 359 398.4 344M307.2 153.6C307.2 68.8 238.4 0 153.6 0 68.8 0 0 68.8 0 153.6c0 84.8 68.8 153.6 153.6 153.6C238.4 307.2 307.2 238.4 307.2 153.6M153.6 268.8c-63.5 0-115.2-51.7-115.2-115.2 0-63.5 51.7-115.2 115.2-115.2 63.5 0 115.2 51.7 115.2 115.2C268.8 217.1 217.1 268.8 153.6 268.8"/><path class="ab1" d="M64,153.6h25.6c0-35.3,28.7-64,64-64V64C104.2,64,64,104.2,64,153.6"/></svg>
								</div>
							</button>
						</div>
						<button class="hamburger hamburger--slider" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</nav>
			</header>
			<section class="hero" data-editable="true" data-component="slider" data-id="2">
				<div class="hee-slider hee-slider-white" style="width: 100%; height: 100%;">
					<div class="slides">
						<?php $__currentLoopData = $component['Slider']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="slide" style="background-image: url('<?php echo e($config['site_url']); ?>data/content_data/<?php echo e($content['picture']); ?>')">
							<div class="content">
								<span class="title"><?php echo e($content['title']); ?></span>
							</div>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
					<div class="controls">
						<a class="control prev" href="javascript:void(0)"></a>
						<a class="control next" href="javascript:void(0)"></a>
					</div>
					<div class="counter"></div>
				</div>
			</section>
			<section class="section center charade" data-editable="true" data-component="text" data-id="3">
				<div class="section-wrapper">
					<div class="section-titles">
						<p class="section-title">Somos uma empresa focada em assessoria de carreira e gestão de atletas prossionais de elite.</p>
					</div>
					<div class="subsection">
						<div class="subsection-titles">
							<p class="subsection-title">MISSÃO</p>
						</div>
						<p class="subsection-text">Proporcionar aos nossos atletas todo o respaldo em todos os âmbitos: profissional, pessoal e financeiro; sendo um porto seguro para os mesmos!</p>
					</div>
					<div class="subsection">
						<div class="subsection-titles">
							<p class="subsection-title">VISÃO</p>
						</div>
						<p class="subsection-text">Tornar-se referência no meio do futebol, não só na grandeza, mas com qualidade e diferenciais nos serviços prestados.</p>
					</div>
				</div>
			</section>
			<section class="section center grey" data-editable="true" data-component="carousel" data-id="4">
				<div class="section-wrapper small-pad">
					<div class="section-titles">
						<p class="section-title">SERVIÇOS</p>
					</div>
					<div class="carousel services">
						<?php $__currentLoopData = $component['CarouselServicos']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="servicescomponent">
							<div class="servicescomponent-picture">
								<img src="<?php echo e($config['site_url']); ?>data/content_data/<?php echo e($content['picture']); ?>">
							</div>
							<div class="servicescomponent-content">
								<p class="servicescomponent-title"><?php echo e($content['title']); ?></p>
								<p class="servicescomponent-description"><?php echo e($content['description']); ?></p>
							</div>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</section>
			<section class="section center pinkyred" data-editable="true" data-component="carousel" data-id="5">
				<div class="section-wrapper small-pad">
					<div class="section-titles">
						<p class="section-title">PORTFÓLIO</p>
					</div>
					<div class="carousel portfolio">
						<?php $__currentLoopData = $component['CarouselPortfolio']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="portfoliocomponent">
							<div class="portfoliocomponent-picture">
								<img src="<?php echo e($config['site_url']); ?>data/content_data/<?php echo e($content['picture']); ?>">
							</div>
							<div class="portfoliocomponent-content">
								<p class="portfoliocomponent-title"><?php echo e($content['title']); ?></p>
							</div>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</section>
			<section class="section center grey" data-editable="true" data-component="carousel" data-id="6">
				<div class="section-wrapper small-pad">
					<div class="section-titles">
						<p class="section-title">CLIENTES | PARCEIROS</p>
					</div>
					<div class="carousel clients">
						<?php $__currentLoopData = $component['CarouselClientesParceiros']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="clientscomponent">
							<div class="clientscomponent-picture">
								<img src="<?php echo e($config['site_url']); ?>data/content_data/<?php echo e($content['picture']); ?>">
							</div>
							<div class="clientscomponent-content">
								<p class="clientscomponent-title"><?php echo e($content['title']); ?></p>
								<p class="clientscomponent-description"><?php echo e($content['description']); ?></p>
							</div>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</section>
			<section class="section center white" data-editable="true" data-component="form" data-id="7">
				<div class="section-wrapper small-pad">
					<div class="section-titles">
						<p class="section-title">Fale conosco</p>
					</div>
					<div class="fale-conosco">
						<div class="inputs">
							<div class="input">
								<label>Nome</label>
								<input type="text" name="nome">
							</div>
							<div class="input">
								<label>E-mail</label>
								<input type="email" name="email">
							</div>
							<div class="input">
								<label>Telefone</label>
								<input type="text" name="Telefone">
							</div>
							<div class="input">
								<label>Assunto</label>
								<input type="text" name="assunto">
							</div>
							<div class="input">
								<label>Mensagem</label>
								<textarea></textarea>
							</div>
						</div>
						<a class="btn btn-alpha btn-medium" href="#">Enviar mensagem</a>
					</div>
				</div>
			</section>
		</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
	##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
		<!-- Script -->
		<script type="text/javascript" src="<?php echo e($theme); ?>/vendor/jquery/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="<?php echo e($theme); ?>/vendor/flickity/flickity.pkgd.min.js"></script>
		<script type="text/javascript" src="<?php echo e($theme); ?>/vendor/gsap/TweenMax.min.js"></script>
		<script type="text/javascript" src="<?php echo e($theme); ?>/vendor/gsap/TimelineMax.min.js"></script>
		<script type="text/javascript" src="<?php echo e($theme); ?>/vendor/heeSlider/heeSliderCSS.js"></script>
		<script type="text/javascript">
		var carousel = document.querySelectorAll('.carousel');
		[].forEach.call(carousel, function (el, i) {new Flickity(el, {
			  // options
			  groupCells: true,
			  setGallerySize: true,
			  contain: true,
			  freeScroll: true,
			  adaptiveHeight: true,
			});
		})
		</script>
		<script type="text/javascript">
		const slider = new heeSlider(document.querySelector('.hee-slider'))

		$(document).ready(function() {
			var el = document.querySelector('.rise')

			var request = null;
			var mouse = {
				x: 0,
				y: 0
			};
			var cx = el.offsetWidth / 2;
			var cy = el.offsetHeight / 2;

			$('.rise').mousemove(function(event) {

				mouse.x = event.pageX;
				mouse.y = event.pageY;

				cancelAnimationFrame(request);
				request = requestAnimationFrame(update);
			});

			function update() {

				dx = mouse.x - cx;
				dy = mouse.y - cy;

				tiltx = (dy / cy);
				tilty = -(dx / cx);
				radius = Math.sqrt(Math.pow(tiltx, 4) + Math.pow(tilty, 4));
				degree = (radius * 0.5);
				TweenMax.to(".rise", 1, {
					//transform: 'rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)',
					ease: Power2.easeOut
				});
			}

			$(window).resize(function() {
				cx = window.innerWidth / 2;
				cy = window.innerHeight / 2;
			});
		});
		</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>