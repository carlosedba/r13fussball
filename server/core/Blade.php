<?php
namespace Rise;

class Blade extends \Slim\Views\Blade
{

    /**
     * @param Blade
     */
	protected $renderer;
	
	public function __construct($viewPaths = [], $cachePath = '', Dispatcher $events = null, $attributes = [])
	{
		parent::__construct($viewPaths, $cachePath, $events, $attributes);

		$this->renderer = new \Philo\Blade\Blade($this->viewPaths, $this->cachePath, $this->events);
	}

    /**
     * @return Blade
     */
	public function getRenderer()
	{
		return $this->renderer;
	}

    /**
     * Shortcut for adding custom directives to Blade.
     *
     * @param string $name
     * @param callable $handler
     *
     * @return void
     */
    public function directive($name, callable $handler)
    {
        $compiler = $this->getRenderer()->getCompiler();
        return $compiler->directive($name, $handler);
    }

    /**
     * Renders a template and returns the result as a string
     *
     * cannot contain template as a key
     *
     * throws RuntimeException if $templatePath . $template does not exist
     *
     * @param $template
     * @param array $data
     *
     * @return mixed
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function fetch($template, array $data = [])
    {
        if (isset($data['template'])) {
            throw new \InvalidArgumentException("Duplicate template key found");
        }

        $data = array_merge($this->attributes, $data);

        return $this->renderer->view()->make($template, $data)->render();
    }

}
?>
