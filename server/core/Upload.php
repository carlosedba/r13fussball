<?php
namespace Rise;

use Rise\Utils\IdGenerator;
use \Slim\Http\UploadedFile;

class Upload
{
	static function saveTo($directory, UploadedFile $uploadedFile)
	{
		$extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
		$basename = IdGenerator::uniqueId(8);
		$filename = sprintf('%s.%0.8s', $basename, $extension);

		$uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

		return $filename;
	}
}
?>