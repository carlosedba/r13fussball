<?php
namespace Rise;

use \PDO;
use \ORM;
use \Model;

class Container extends \Slim\Container
{	
	public function __construct(array $values = [])
	{
		parent::__construct(array('settings' => $values['config']['netcore']));

		$this->setDatabase();
		$this->setApplicationRenderer();
		$this->setThemeRenderer();
		$this->setDirectives();
	}

	public function setDatabase()
	{
		$settings = $this->get('settings');

		ORM::configure("{$settings['db']['driver']}:host={$settings['db']['host']};dbname={$settings['db']['database']}");
		ORM::configure('username', $settings['db']['username']);
		ORM::configure('password', $settings['db']['password']);
		ORM::configure('return_result_sets', true);
		ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		Model::$auto_prefix_models = 'Rise\\Models\\';
		Model::$short_table_names = true;
	}

	public function setApplicationRenderer()
	{
		$this->offsetSet('rise', function () {
			$settings = $this->get('settings');
			$templates = $settings['renderer']['rise']['templates'];
			$cache = $settings['renderer']['rise']['cache'];

			return new Blade($templates, $cache);
		});
	}

	public function setThemeRenderer()
	{
		$this->offsetSet('theme', function () {
			$settings = $this->get('settings');
			$templates = $settings['renderer']['theme']['templates'];
			$cache = $settings['renderer']['theme']['cache'];

			return new Blade($templates, $cache);
		});
	}

	public function setDirectives()
	{
		$rise = $this->get('rise');
		$theme = $this->get('theme');

		$rise->directive('wow', function ($expression) {
            return "<?php echo 'lalala'; ?>";
        });
	}
}
?>
