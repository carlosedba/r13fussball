CREATE DATABASE IF NOT EXISTS `rise` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `rise`;

CREATE TABLE IF NOT EXISTS `rise_config` (
	`id`				int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	`value`				varchar(255) 	NOT NULL,
	`autoload`			tinyint(1) 		NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_meta` (
	`id`				int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	`value`				varchar(255) 	NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_components` (
	`id`				int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	`type`				varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_content` (
	`id`				varchar(255)    NOT NULL,
	`id_component`		int    			NOT NULL,
	`title`				varchar(255),
	`description`		varchar(255),
	`category`			varchar(255),
	`content`			text,
	`url`				varchar(255),
	`picture`			varchar(255),
	`order`				varchar(255),
	`status`			varchar(255),
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`id_component`) REFERENCES `rise_components` (`id`)
	ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_users` (
	`id`				varchar(255)    NOT NULL,
	`first_name`		varchar(255)	NOT NULL,
	`last_name`			varchar(255)	NOT NULL,
	`email`				varchar(255)	NOT NULL,
	`password`			varchar(255)	NOT NULL,
	`picture`			varchar(255),
	`scope`				text			,
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_pages` (
	`id`				varchar(255)    NOT NULL,
	`title`				varchar(255)	NOT NULL,
	`description`		varchar(255),
	`category`			varchar(255)	NOT NULL,
	`content`			text,
	`cover_picture`		varchar(255),
	`status`			varchar(255)	NOT NULL,
	`url`				varchar(255) 	NOT NULL,
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_posts` (
	`id`				varchar(255)    NOT NULL,
	`id_user`			varchar(255)    NOT NULL,
	`title`				varchar(255)	NOT NULL,
	`description`		varchar(255),
	`category`			varchar(255)	NOT NULL,
	`content`			text,
	`cover_picture`		varchar(255),
	`status`			varchar(255)	NOT NULL,
	`url`				varchar(255) 	NOT NULL,
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`id_user`) REFERENCES `rise_users` (`id`)
	ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_url', 'http://localhost:8080/', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_home', 'http://localhost:8080/', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_name', 'R13 Fussball', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_description', '', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('admin_email', 'carlosedba@outlook.com', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('theme', 'r13fussball', '1');

INSERT INTO `rise_meta` (`name`, `value`) VALUES ('site_users', '1');
INSERT INTO `rise_meta` (`name`, `value`) VALUES ('site_pages', '0');
INSERT INTO `rise_meta` (`name`, `value`) VALUES ('site_posts', '0');

INSERT INTO `rise_components` (`name`, `type`) VALUES ('Menu', 'menu');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Slider', 'slider');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Missão/Visão', 'text');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Carousel Serviços', 'carousel');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Carousel Portfólio', 'carousel');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Carousel Clientes/Parceiros', 'carousel');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Fale conosco', 'form');

INSERT INTO `rise_content` (`id`, `id_component`, `title`, `description`, `picture`, `order`, `status`) VALUES ('b6a18a10', '4', 'Braços da empresa pelo mundo', 'Ex jogadores de diversas partes do mundo como: Europa, Asia, América do Norte e América Central.', 'ic_mundo.png', '1', 'visible');
INSERT INTO `rise_content` (`id`, `id_component`, `title`, `description`, `picture`, `order`, `status`) VALUES ('472e3442', '4', 'Preparador físico', 'Próprio da empresa e uso das unidades da Uplay Fitness durante período de férias ou transição de clubes.', 'ic_preparador.png', '2', 'visible');
INSERT INTO `rise_content` (`id`, `id_component`, `title`, `description`, `picture`, `order`, `status`) VALUES ('fc60c846', '4', 'Acessoria financeira e de investimentos', '', 'ic_investimentos.png', '3', 'visible');
INSERT INTO `rise_content` (`id`, `id_component`, `title`, `description`, `picture`, `order`, `status`) VALUES ('673d621e', '4', 'Convênio com a Nike para materiais e patrocínio para determinados atletas', '', 'ic_nike.png', '4', 'visible');

INSERT INTO `rise_content` (`id`, `id_component`, `title`, `picture`, `order`, `status`) VALUES ('d726bf0a', '2', 'Teste', '1f6cecc1.jpg', '1', 'visible');
INSERT INTO `rise_content` (`id`, `id_component`, `title`, `picture`, `order`, `status`) VALUES ('525efd46', '2', 'Teste 2', '2bc5521f.jpg', '2', 'visible');
INSERT INTO `rise_content` (`id`, `id_component`, `title`, `picture`, `order`, `status`) VALUES ('be7cd9aa', '2', 'Teste 3', '1ffd4d31.jpg', '3', 'visible');
INSERT INTO `rise_content` (`id`, `id_component`, `title`, `picture`, `order`, `status`) VALUES ('9aea7f55', '2', 'Teste 4', '4457dfd4.jpg', '4', 'visible');
INSERT INTO `rise_content` (`id`, `id_component`, `title`, `picture`, `order`, `status`) VALUES ('07dcf5a3', '2', 'Teste 5', '18df16b4.jpg', '5', 'visible');

INSERT INTO `rise_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`, `scope`) VALUES ('0cbdeb00', 'Carlos Eduardo', 'Barbosa de Almeida', 'carlosedba@outlook.com', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'eu.jpg', '');
