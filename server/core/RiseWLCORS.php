<?php
namespace Rise;

require_once('Bootstrap.php');

use \Model;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Tuupola\Middleware\Cors as CORS;
use Rise\Auth\JWT;
use Rise\Models\User;

$container = new Container($config);
$net = new Netcore($container);

$net->add(new CORS([
	"origin" => ["*"],
	"methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
	"headers.allow" => ['X-Requested-With', 'Content-Type', 'Accept', 'Origin', 'Authorization'],
	"headers.expose" => [],
	"credentials" => true,
	"cache" => 0
]);

$net->add(new JWT([
	"secure" => false,
	"signature" => "RSA",
	"algorithm" => "SHA256",
	"key" => PUBLIC_KEY,
	"path" => ["/admin", "/api", "/wow"],
	"ignore" => ["/api/token"],
	"callback" => function ($request, $response, $args) use ($container) {
		$container->set('token', $args['token']);
		return $response;
	},
	"error" => function ($request, $response, $args) {
		return $response->withRedirect($request->getUri()->getBasePath() . '/login');
	}
]));

$net->group('/api', function () {
	$this->delete('/users/delete/{id}[/]', 			constant('NAMESPACE') . '\Api\Users:delete');
	$this->put('/users/update/{id}[/]', 			constant('NAMESPACE') . '\Api\Users:update');
	$this->post('/users/create[/]', 				constant('NAMESPACE') . '\Api\Users:create');	
	$this->get('/users/limit[/]',					constant('NAMESPACE') . '\Api\Users:limit');
	$this->get('/users/email/{email}[/]', 			constant('NAMESPACE') . '\Api\Users:findOne');
	$this->get('/users/{id}[/]', 					constant('NAMESPACE') . '\Api\Users:findOneById');
	$this->get('/users[/]', 						constant('NAMESPACE') . '\Api\Users:findAll');

	$this->delete('/config/delete/{id}[/]', 		constant('NAMESPACE') . '\Api\Config:delete');
	$this->put('/config/update/{id}[/]', 			constant('NAMESPACE') . '\Api\Config:update');
	$this->post('/config/create[/]', 				constant('NAMESPACE') . '\Api\Config:create');	
	$this->get('/config/{id}[/]', 					constant('NAMESPACE') . '\Api\Config:findOneById');
	$this->get('/config[/]', 						constant('NAMESPACE') . '\Api\Config:findAll');

	$this->delete('/components/delete/{id}[/]', 	constant('NAMESPACE') . '\Api\Components:delete');
	$this->put('/components/update/{id}[/]', 		constant('NAMESPACE') . '\Api\Components:update');
	$this->post('/components/create[/]', 			constant('NAMESPACE') . '\Api\Components:create');	
	$this->get('/components/{id}[/]', 				constant('NAMESPACE') . '\Api\Components:findOneById');
	$this->get('/components[/]', 					constant('NAMESPACE') . '\Api\Components:findAll');

	$this->delete('/content/delete/{id}[/]', 	constant('NAMESPACE') . '\Api\Content:delete');
	$this->put('/content/update/{id}[/]', 		constant('NAMESPACE') . '\Api\Content:update');
	$this->post('/content/create[/]', 			constant('NAMESPACE') . '\Api\Content:create');	
	$this->get('/content/{id}[/]', 				constant('NAMESPACE') . '\Api\Content:findOneById');
	$this->get('/content[/]', 					constant('NAMESPACE') . '\Api\Content:findAll');
});

$net->get('/', function (Request $request, Response $response, $args) {
	$token = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, array("wow" => "teste", "lalala" => "hehehe"));
	$isLegit = JWT::verify('RSA', 'SHA256', PUBLIC_KEY, $token);

	return $this->rise->render($response, 'wow', []);

	/*$response->getBody()->write("Hello");
	return $response;*/
});

$net->post('/api/token', function (Request $request, Response $response, $args) {
	$data = $request->getParsedBody();
	$email = $data['email'];
	$password = $data['password'];
	$factory = Model::factory('User');

	if ($user = $factory->where('email', $email)->findOne()) {
		if ($user->password == hash('sha256', $password)) {
			$token = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, ['id' => $user->id])->__toString();
			$json = json_encode(array('token' => $token));
			$response->getBody()->write($json);

			return $response;	
		} else {
			$json = json_encode(array(
				'error' => '1001',
				'message' => 'Invalid password.'
			));
			$response->getBody()->write($json);
		}
	} else {
		$json = json_encode(array(
			'error' => '1002',
			'message' => 'Email not found.'
		));
		$response->getBody()->write($json);
	}

	return $response;
});

$net->get('/wow', function (Request $request, Response $response, $args) {
	$response->getBody()->write("AUTHORIZEDDDDDDDDDDDDD");
	return $response;
});

$net->run();
?>
