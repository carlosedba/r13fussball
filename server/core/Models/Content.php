<?php
namespace Rise\Models;

use Rise\Model;

class Content extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_component', 'title', 'description', 'category', 'content', 'url', 'picture', 'order', 'status',
    ];

    public function component()
    {
        return $this->belongsTo('Component', 'id_component');
    }
}
?>