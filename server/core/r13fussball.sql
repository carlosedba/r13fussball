CREATE DATABASE IF NOT EXISTS `r13fussball` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `r13fussball`;

CREATE TABLE IF NOT EXISTS `rise_config` (
	`id`				int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	`value`				varchar(255) 	NOT NULL,
	`autoload`			tinyint(1) 		NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_meta` (
	`id`				int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	`value`				varchar(255) 	NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_components` (
	`id`				int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	`type`				varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_content` (
	`id`				varchar(255)    NOT NULL,
	`id_component`		int    			NOT NULL,
	`title`				varchar(255),
	`description`		varchar(255),
	`category`			varchar(255),
	`content`			text,
	`url`				varchar(255),
	`picture`			varchar(255),
	`order`				varchar(255),
	`status`			varchar(255),
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`id_component`) REFERENCES `rise_components` (`id`)
	ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_users` (
	`id`				varchar(255)    NOT NULL,
	`first_name`		varchar(255)	NOT NULL,
	`last_name`			varchar(255)	NOT NULL,
	`email`				varchar(255)	NOT NULL,
	`password`			varchar(255)	NOT NULL,
	`picture`			varchar(255),
	`scope`				text			,
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_pages` (
	`id`				varchar(255)    NOT NULL,
	`title`				varchar(255)	NOT NULL,
	`description`		varchar(255),
	`category`			varchar(255)	NOT NULL,
	`content`			text,
	`cover_picture`		varchar(255),
	`status`			varchar(255)	NOT NULL,
	`url`				varchar(255) 	NOT NULL,
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_posts` (
	`id`				varchar(255)    NOT NULL,
	`id_user`			varchar(255)    NOT NULL,
	`title`				varchar(255)	NOT NULL,
	`description`		varchar(255),
	`category`			varchar(255)	NOT NULL,
	`content`			text,
	`cover_picture`		varchar(255),
	`status`			varchar(255)	NOT NULL,
	`url`				varchar(255) 	NOT NULL,
	`created_at`		datetime,
	`updated_at`		datetime,
	`deleted_at`		datetime,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`id_user`) REFERENCES `rise_users` (`id`)
	ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_url', 'http://localhost:8181/', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_home', 'http://localhost:8181/', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_name', 'R13 Fussball', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_description', '', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('admin_email', 'carlosedba@outlook.com', '1');
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('theme', 'r13fussball', '1');

INSERT INTO `rise_meta` (`name`, `value`) VALUES ('site_users', '1');
INSERT INTO `rise_meta` (`name`, `value`) VALUES ('site_pages', '0');
INSERT INTO `rise_meta` (`name`, `value`) VALUES ('site_posts', '0');

INSERT INTO `rise_components` (`name`, `type`) VALUES ('Menu', 'menu');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Slider', 'slider');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Missão/Visão', 'text');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Carousel Serviços', 'carousel');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Carousel Portfólio', 'carousel');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Carousel Clientes/Parceiros', 'carousel');
INSERT INTO `rise_components` (`name`, `type`) VALUES ('Fale conosco', 'form');

INSERT INTO `rise_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`, `scope`) VALUES ('0cbdeb00', 'Carlos Eduardo', 'Barbosa de Almeida', 'carlosedba@outlook.com', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'eu.jpg', '');
INSERT INTO `rise_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`, `scope`) VALUES ('1cbdeb11', 'Bruno', 'Kubiak', 'brunomkubiak@gmail.com', 'fa81d222411bc0abea538c5a90b10832d90188366d63b13a613fd6de7858874b', 'bruno.jpg', '');

INSERT INTO `rise_content` (`id`, `id_component`, `title`, `description`, `category`, `content`, `url`, `picture`, `order`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
('09cd31aa', 2, 'Otimização de jogadores', NULL, NULL, NULL, NULL, '24c443f2.png', NULL, 'visible', NULL, NULL, NULL),
('ee315a2c', 2, 'Planejamento de carreira', NULL, NULL, NULL, NULL, '8d8ca836.png', NULL, 'visible', NULL, NULL, NULL),
('f765acb9', 2, 'Marketing Esportivo', NULL, NULL, NULL, NULL, 'b0291613.png', NULL, 'visible', NULL, NULL, NULL),
('6cf34bc4', 2, 'Acompanhamento técnico', NULL, NULL, NULL, NULL, '57287cb3.png', NULL, 'visible', NULL, NULL, NULL),

('27c92aea', 4, 'Fisioterapeuta próprio', 'Disponibilizaremos aos nossos atletas quando o atleta atue em países onde a fisioterapia não seja tão desenvolvida.', NULL, NULL, NULL, '27ab50fa.png', NULL, 'visible', NULL, NULL, NULL),
('3a14028b', 4, 'Coaching', 'Para otimização do\r\nlado psicológico do atleta', NULL, NULL, NULL, '358fecad.png', NULL, 'visible', NULL, NULL, NULL),
('472e3442', 4, 'Preparador físico', 'Próprio da empresa e uso das unidades da Uplay Fitness durante período de férias ou transição de clubes.', NULL, NULL, NULL, 'a9d27303.png', '2', 'visible', NULL, NULL, NULL),
('673d621e', 4, 'Convênio com a Nike ', 'Materiais e patrocínio para determinados atletas', NULL, NULL, NULL, '6f663283.png', '4', 'visible', NULL, NULL, NULL),
('af5e3570', 4, 'Assessoria de imagem', 'Gerenciamento de redes sociais, criação de logo, patrocínios.', NULL, NULL, NULL, '9b66b7c0.png', NULL, 'visible', NULL, NULL, NULL),
('b502c9b1', 4, 'Assessoria Jurídica', '', NULL, NULL, NULL, '2bc9819c.png', NULL, 'visible', NULL, NULL, NULL),
('b6a18a10', 4, 'Braços da empresa pelo mundo', 'Ex jogadores de diversas partes do mundo como: Europa, Asia, América do Norte e América Central.', NULL, NULL, NULL, '5c696b84.png', '1', 'visible', NULL, NULL, NULL),
('c27b227d', 4, 'Suplementação para atletas', 'Parceria firmada com a\r\nNutrilatina.', NULL, NULL, NULL, '8a7ba6a3.png', NULL, 'visible', NULL, NULL, NULL),
('fc60c846', 4, 'Acessoria financeira e de investimentos', 'Para um crescimento dentro  e fora do campo, assegurando o melhor rendimento financeiro para o futuro do atleta', NULL, NULL, NULL, '89da894e.png', '3', 'visible', NULL, NULL, NULL),

('6de56447', 5, 'Identidade Visual', 'Logo criada para jogador Giovanni do Náutico', NULL, NULL, NULL, 'dbde1d96.png', NULL, 'visible', NULL, NULL, NULL),
('7f8a96e2', 5, 'Identidade Visual W18', 'Logo criada para jogador Walter do Atlético Goianiense', NULL, NULL, NULL, '791c77ea.png', NULL, 'visible', NULL, NULL, NULL),
('8993cd67', 5, 'Identidade visual', 'Logo criada para jogador Marcinho do Londrina. ', NULL, NULL, NULL, 'a8013839.png', NULL, 'visible', NULL, NULL, NULL),
('9bb70e28', 5, 'Identidade Visual R13 e Redes sociais.', 'Logo criada para jogador Rafinha do bayern de Munique. Manutenção de redes sociais (instagram e facebook).', NULL, NULL, NULL, '2aac1c81.png', NULL, 'visible', NULL, NULL, NULL),
('24344a6b', 5, 'Identidade Visual e manutenção de mídias sociais', 'Logo criada para jogador Robinho do Londrina, atualmente empresado para o Ferroviário . Manutenção de redes sociais do jogador.', NULL, NULL, NULL, 'f8ce67f1.png', NULL, 'visible', NULL, NULL, NULL),
('d6f44cc7', 5, 'Identidade Visual ', 'Logo criada para jogador Walter do Londrina. ', NULL, NULL, NULL, '175f3c0e.png', NULL, 'visible', NULL, NULL, NULL),

('c38808ec', 6, 'Nelsinho', 'Jogador do Arouca - Portugal', NULL, NULL, NULL, 'dfb594fa.png', NULL, 'visible', NULL, NULL, NULL),
('ccced386', 6, 'Robinho', 'Jogador do Londrina, emprestado para o Ferroviário', NULL, NULL, NULL, '35d3aa2a.png', NULL, 'visible', NULL, NULL, NULL),
('d4908bd9', 6, 'Marcinho', 'Jogador do Londrina.', NULL, NULL, NULL, '2c890f18.png', NULL, 'visible', NULL, NULL, NULL),
('de047b99', 6, 'Nike', 'Fornecedora de equipamentos e acessórios para nossos atletas', NULL, NULL, NULL, '9c32ea23.png', NULL, 'visible', NULL, NULL, NULL),
('ffa3858d', 6, 'Rafinha', 'Jogador do Bayern de Munique, já atuou na Seleção brasleira diversas vezes. ', NULL, NULL, NULL, 'eb7c96bf.png', NULL, 'visible', NULL, NULL, NULL),
('afc3fbec', 6, 'Nutrilatina', 'Fornecedora de suplementos para nossos atletas', NULL, NULL, NULL, '120d985a.png', NULL, 'visible', NULL, NULL, NULL),
('0d2ac1ef', 6, 'Giovanni Piccolomo', 'Jogador do Náutico', NULL, NULL, NULL, 'f49f67f2.png', NULL, 'visible', NULL, NULL, NULL),
('10b21705', 6, 'Walter', 'Jogador do Atlético goianiense', NULL, NULL, NULL, 'd2fa20c2.png', NULL, 'visible', NULL, NULL, NULL);





