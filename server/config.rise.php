<?php
return [
	'config' => [
		'production' => false,

		'netcore' => [
			'displayErrorDetails' => true, // set to false in production
			'addContentLengthHeader' => false, // Allow the web server to send the content-length header
			'determineRouteBeforeAppMiddleware' => false,

			// Renderer settings
			'renderer' => [
				'rise' => [
					'cache' => CACHE_PATH,
					'templates' => TEMPLATES_PATH,
				],

				'theme' => [
					'cache' => CACHE_PATH,
					'templates' => THEMES_PATH,
				]
			],

			'db' => [
				'driver' 		=> 'mysql',
				'host' 			=> 'localhost:3306',
				'database' 		=> 'r13fussball',
				'username'	 	=> 'CarlosEDBA',
				'password' 		=> 'AquelaSenhaMarota',
				'charset'   	=> 'utf8mb4',
				'collation' 	=> 'utf8mb4_unicode_ci',
				'prefix'    	=> 'rise_',
			],
		],

		// Monolog settings
		'logger' => [
			'name' => 'rise',
			'path' => LOGS_PATH . '/rise.log',
			'level' => \Monolog\Logger::DEBUG,
		],
	],
];

