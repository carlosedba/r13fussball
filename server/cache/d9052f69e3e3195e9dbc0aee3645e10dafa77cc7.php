<head>
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php $__env->startSection('css'); ?>
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/vendor/normalize/normalize.css">
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/vendor/hamburgers/hamburgers.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/vendor/flickity/flickity.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/vendor/heeSlider/hee-slider.css">
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/resources/css/colors.css">
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/resources/css/buttons.css">
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/resources/css/inputs.css">
		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/resources/css/main.css">
  		<link rel="stylesheet" type="text/css" href="<?php echo e($theme); ?>/static/css/app.26ef044608b2551213a88a28fac58c11.css"/>
  		<!-- Fonts -->
  		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
  		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
		<?php echo $__env->yieldSection(); ?>
	<?php $__env->startSection('js'); ?>
		<script src="<?php echo e($theme); ?>/vendor/modernizr-2.8.3.min.js"></script>
		<script src="<?php echo e($theme); ?>/vendor/bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
		<?php echo $__env->yieldSection(); ?>
</head>