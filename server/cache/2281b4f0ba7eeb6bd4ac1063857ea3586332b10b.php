<!DOCTYPE html>
<html lang="pt-BR">
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<body>
		<?php $__env->startSection('content'); ?>
			<?php echo $__env->yieldSection(); ?>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php echo $__env->make('includes.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</body>
</html>