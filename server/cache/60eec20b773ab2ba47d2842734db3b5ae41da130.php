<head>
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php $__env->startSection('css'); ?>
		<?php echo $__env->yieldSection(); ?>
	<?php $__env->startSection('js'); ?>
		<script src="<?php echo e($theme); ?>/vendor/modernizr-2.8.3.min.js"></script>
		<script src="<?php echo e($theme); ?>/vendor/bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
		<?php echo $__env->yieldSection(); ?>
</head>