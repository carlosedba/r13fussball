export default class Event {
	constructor() {
		console.log('log > Event - initialized!')
		
		this.handlers = Object.create(Object.prototype)
	}

	getHandler(name) {
		return this.handlers[name]
	}

	addHandler(name, func, context = window) {
		return Object.defineProperty(this.handlers, name, { value: func.bind(context) })
	}

	removeHandler(name, func) {
		return delete this.handlers[name]
	}

	addTo(node, eventType, handlerName, propagation = false) {
		if (node !== undefined, eventType !== undefined, handlerName !== undefined) {
			if (node.length === undefined) {
				node.addEventListener(eventType, this.getHandler(handlerName))
			} else {
				let nodes = [].slice.call(node)
				nodes.map(function (el, i) { el.addEventListener(eventType, this.getHandler(handlerName)), propagation }.bind(this))
			}
		}
	}

	removeFrom(node, eventType, handlerName, propagation = false) {
		if (node !== undefined, eventType !== undefined, handlerName !== undefined) {
			if (node.length === undefined) {
				node.removeEventListener(eventType, this.getHandler(handlerName))
			} else {
				let nodes = [].slice.call(node)
				nodes.map(function (el, i) { el.removeEventListener(eventType, this.getHandler(handlerName)), propagation }.bind(this))
			}
		}
	}
}
