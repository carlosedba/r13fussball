import Transformer from './transformer';

export default class AccountTransformer extends Transformer {
  static fetch(config) {
    return {
      email: config.email,
      firstName: config.first_name,
      lastName: config.last_name,
    };
  }

  static send(config) {
    return {
      email: config.email,
      first_name: config.firstName,
      last_name: config.lastName,
    };
  }
}
