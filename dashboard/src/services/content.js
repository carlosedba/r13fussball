import store from '@/store'
import { call } from '@/services'
import * as types from '@/store/modules/content/mutation-types'

const namespace = 'content'

export const netFindContents = (params) => call(`${namespace}/${types.NET_FIND_CONTENTS}`, params)
export const netFindContent = (params) => call(`${namespace}/${types.NET_FIND_CONTENT}`, params)
export const netCreateContent = (params) => call(`${namespace}/${types.NET_CREATE_CONTENT}`, params)
export const netUpdateContent = (params) => call(`${namespace}/${types.NET_UPDATE_CONTENT}`, params)
export const netRemoveContent = (params) => call(`${namespace}/${types.NET_REMOVE_CONTENT}`, params)
export const updateContentOrder = (params) => store.dispatch(`${namespace}/${types.UPDATE_CONTENT_ORDER}`, params)
