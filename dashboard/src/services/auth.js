import store from '@/store'
import { call } from '@/services'
import * as types from '@/store/modules/auth/mutation-types'

const namespace = 'auth'

export const check = () => store.dispatch(`${namespace}/${types.CHECK}`)
export const login = (params) => call(`${namespace}/${types.LOGIN}`, params)
export const logout = () => store.dispatch(`${namespace}/${types.LOGOUT}`)
export const confirmPassword = (params) => store.dispatch(`${namespace}/${types.CONFIRM_PASSWORD}`, params)
