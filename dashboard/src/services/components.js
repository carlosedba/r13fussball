import store from '@/store'
import { call } from '@/services'
import * as types from '@/store/modules/components/mutation-types'

const namespace = 'components'

export const findComponents = () => call(`${namespace}/${types.FIND_COMPONENTS}`)
export const findComponent = (params) => call(`${namespace}/${types.FIND_COMPONENT}`, params)
export const createComponent = (params) => call(`${namespace}/${types.CREATE_COMPONENT}`, params)
export const updateComponent = (params) => call(`${namespace}/${types.UPDATE_COMPONENT}`, params)
export const removeComponent = (params) => call(`${namespace}/${types.REMOVE_COMPONENT}`, params)
