import store from '@/store'

/*
store.subscribe((mutation, state) => {
	console.log('subscribed', mutation)
})
*/

export const call = (action, payload) => {
	return new Promise((resolve, reject) => {
		store.dispatch(action, payload)
			.then(() => {
				store.subscribe((mutation, state) => {
					if (mutation.type === `${action}_SUCCEEDED`) resolve(state)
					if (mutation.type === `${action}_FAILED`) reject(state)
				})
			})
	})
}