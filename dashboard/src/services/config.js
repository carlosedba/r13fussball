import store from '@/store'
import { call } from '@/services'
import * as types from '@/store/modules/config/mutation-types'

const namespace = 'config'

export const findConfigs = () => call(`${namespace}/${types.FIND_CONFIGS}`)
export const findConfig = (params) => call(`${namespace}/${types.FIND_CONFIG}`, params)
export const updateConfig = (params) => call(`${namespace}/${types.UPDATE_CONFIG}`, params)
