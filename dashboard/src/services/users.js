import store from '@/store'
import { call } from '@/services'
import * as types from '@/store/modules/users/mutation-types'

const namespace = 'users'

export const netFindUsers = () => call(`${namespace}/${types.NET_FIND_USERS}`)
export const netFindUser = (params) => call(`${namespace}/${types.NET_FIND_USER}`, params)
export const netCreateUser = (params) => call(`${namespace}/${types.NET_CREATE_USER}`, params)
export const netUpdateUser = (params) => call(`${namespace}/${types.NET_UPDATE_USER}`, params)
export const netRemoveUser = (params) => call(`${namespace}/${types.NET_REMOVE_USER}`, params)
