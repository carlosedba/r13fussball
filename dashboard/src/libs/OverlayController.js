export default class Rise {
	constructor() {
		//
	}

  static applyOverride() {
    if (!document.body.classList.contains('rise-override')) document.body.classList.add('rise-override')
    return this
  }

  static removeOverride() {
    if (document.body.classList.contains('rise-override')) document.body.classList.remove('rise-override')
    return this
  }
}