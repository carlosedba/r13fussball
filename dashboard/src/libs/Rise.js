import ModalController from './ModalController'
//import OverlayController from './OverlayController'

export default class Rise {
	constructor(store = null, router = null) {
    this.init()

    this.store = store
    this.router = router
		this.controllers = {
      modal: new ModalController({ store, router }),
      //overlay: new OverlayController(),
    }
	}

  init() {
    if (document.querySelector('body > .rise-hooks') === null) {
      let div = document.createElement('div')
      div.classList.add('rise-hooks')
      document.body.appendChild(div)
    }
  }

  modalController() {
    return this.controllers.modal
  }

  overlayController() {
    return this.controllers.overlay
  }

  static applyOverride() {
    if (!document.body.classList.contains('rise-override')) document.body.classList.add('rise-override')
    return this
  }

  static removeOverride() {
    if (document.body.classList.contains('rise-override')) document.body.classList.remove('rise-override')
    return this
  }
}