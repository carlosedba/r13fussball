import Vue from 'vue'

export default class ModalController {
	constructor(options = {}) {
    this.options = {
      store: options.store,
      router: options.router,
      root: options.root || '.rise-hooks',
      container: options.container || '.rise-modals',
    }

    this.root = document.querySelector(this.options.root)
    this.container = this.root.querySelector(this.options.container)
		this.cache = Object.create(Object.prototype)

    this.setContainer()
	}

  setContainer() {
    if (this.container === null && this.root) {
      let container = document.createElement('div')
      container.classList.add(this.options.container.substring(1))
      this.root.appendChild(container)
      this.container = container
    }
  }

  init(id, options, mount = null) {
    if (id !== null && id !== undefined && id.length) {
      if (this.cache[id] === null || this.cache[id] === undefined) {
        options = Object.assign({}, options, { store: this.options.store, router: this.options.router })
        const instance = new Vue(options).$mount(mount)
        if (mount === null) this.container.appendChild(instance.$el)
        this.cache[id] = instance
      }

      this.cache[id].open()
    }

    return this
  }

  remove(id) {
    this.container.removeChild(this.cache.id.$el)
    delete this.cache.id

    return this
  }
}