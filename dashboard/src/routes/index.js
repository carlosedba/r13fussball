/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */


/**
 * The routes
 *
 * @type {object} The routes
 */
export default [
  // Dashboard
  {
    path: '/dashboard',
    name: 'dashboard.index',
    component: require('@/pages/dashboard/index.vue'),

    // If the user needs to be authenticated to view this page
    meta: {
      auth: true,
    },
  },

  // Login
  {
    path: '/login',
    name: 'login',
    component: require('@/pages/login/index.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },

  // Logout
  {
    path: '/logout',
    name: 'logout',
    component: require('@/pages/logout/index.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      auth: true,
    },
  },

  // Register
  /*
  {
    path: '/register',
    name: 'register.index',
    component: require('@/pages/register/index.vue'),

    // If the user needs to be a guest to view this page
    meta: {
      guest: true,
    },
  },
  */

  // Index
  {
    path: '/',
    name: 'index',
    component: require('@/pages/index/index.vue'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: false,
    },
  },

  // Redirect
  {
    path: '/*',
    redirect: '/',
  },
];
