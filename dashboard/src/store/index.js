/* ============
 * Vuex Store
 * ============
 *
 * The store of the application.
 *
 * http://vuex.vuejs.org/en/index.html
 */

import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import VuexPromiseMiddleware from 'vuex-promise-middleware'

// Modules
import auth from './modules/auth'
import components from './modules/components'
import config from './modules/config'
import content from './modules/content'
import users from './modules/users'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  /**
   * Assign the modules to the store
   */
  modules: {
    auth,
    components,
    config,
    content,
    users,
  },

  /**
   * If strict mode should be enabled
   */
  strict: debug,

  /**
   * Plugins used in the store
   */
  plugins: debug ? [VuexPromiseMiddleware, createLogger()] : [VuexPromiseMiddleware],
})
