/* ============
 * State of the auth module
 * ============
 *
 * The initial state of the auth module.
 */

export default {
  pending: false,
  authenticated: false,
  confirmed: false,
  error: null,
}
