/* ============
 * Mutation types for the account module
 * ============
 *
 * The mutation types that are available
 * on the auth module.
 */

export const CHECK = 'CHECK'
export const LOGIN = 'LOGIN'
export const LOGIN_SUCCEEDED = 'LOGIN_SUCCEEDED'
export const LOGIN_FAILED = 'LOGIN_FAILED'
export const LOGOUT = 'LOGOUT'
export const CONFIRM_PASSWORD = 'CONFIRM_PASSWORD'
export const CONFIRM_PASSWORD_SUCCEEDED = 'CONFIRM_PASSWORD_SUCCEEDED'
export const CONFIRM_PASSWORD_FAILED = 'CONFIRM_PASSWORD_FAILED'