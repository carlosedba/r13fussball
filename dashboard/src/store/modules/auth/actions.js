/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */
 
import Vue from 'vue';
import * as types from './mutation-types'

export default {
	[types.CHECK] ({ commit }) {
  		commit(types.CHECK)
	},

	[types.LOGIN] ({ commit }, { data }) {
		if (data) {
			const url = `/token`

			Vue.$http.post(url, data)
				.then((response) => {
					const data = response.data

					if (!data.error) {
						commit(types.LOGIN_SUCCEEDED, { data })
					} else {
						commit(types.LOGIN_FAILED, { data })
					}
				})
		}
	},

	[types.LOGOUT] ({ commit }) {
  		commit(types.LOGOUT)
	},

	[types.CONFIRM_PASSWORD] ({ commit }, { id, data }) {
		if (data) {
			const url = `/confirm/password/${id}`

			Vue.$http.post(url, data)
				.then((response) => {
					const data = response.data

					if (!data.error) {
						commit(types.CONFIRM_PASSWORD_SUCCEEDED, { data })
					} else {
						commit(types.CONFIRM_PASSWORD_FAILED, { data })
					}
				})
		}
	},
}