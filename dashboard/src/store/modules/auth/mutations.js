/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from 'vue'
import * as types from './mutation-types'

export default {
  [types.CHECK](state) {
    state.authenticated = !!localStorage.getItem('token')
    if (state.authenticated) {
      Vue.$http.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('token')}`
    }
  },

  [types.LOGIN](state, payload) {
    state.pending = true
  },

  [types.LOGIN_SUCCEEDED](state, payload) {
    let token = payload.data.token

    state.pending = false
    state.authenticated = true

    localStorage.setItem('token', token)
    Vue.$http.defaults.headers.common.Authorization = `Bearer ${token}`
  },

  [types.LOGIN_FAILED](state, payload) {
    state.pending = false
    state.authenticated = false
    state.error = payload.data
  },

  [types.LOGOUT](state) {
    state.authenticated = false
    localStorage.removeItem('token')
    Vue.$http.defaults.headers.common.Authorization = ''
  },

  [types.CONFIRM_PASSWORD](state, payload) {
    state.pending = true
    state.confirmed = false
  },

  [types.CONFIRM_PASSWORD_SUCCEEDED](state, payload) {
    state.pending = false
    state.confirmed = true
  },

  [types.CONFIRM_PASSWORD_FAILED](state, payload) {
    state.pending = false
    state.confirmed = false
    state.error = payload.data
  },
}
