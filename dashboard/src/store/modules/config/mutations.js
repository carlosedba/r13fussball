/* ============
 * Mutations for the config module
 * ============
 *
 * The mutations that are available on the
 * config module.
 */

import * as types from './mutation-types'

export default {
  /*
   * FIND_CONFIGS
   *
   */
  [types.FIND_CONFIGS](state) {
    state.list.pending = true
  },

  [types.FIND_CONFIGS_SUCCEEDED](state, payload) {
    let data = payload.data
    state.list.data = data
    state.list.pending = false
  },

  [types.FIND_CONFIGS_FAILED](state, payload) {
    let data = payload.data
    state.list.pending = false
    state.list.error = data
  },


  /*
   * FIND_CONFIG
   *
   */
  [types.FIND_CONFIG](state) {
    state.current.pending = true
  },

  [types.FIND_CONFIG_SUCCEEDED](state, payload) {
    let data = payload.data
    state.current.data = data
    state.current.pending = false
  },

  [types.FIND_CONFIG_FAILED](state, payload) {
    let data = payload.data
    state.current.pending = false
    state.current.error = data
  },


  /*
   * UPDATE_CONFIG
   *
   */
  [types.UPDATE_CONFIG](state) {
    state.updated.pending = true
  },

  [types.UPDATE_CONFIG_SUCCEEDED](state, payload) {
    let data = payload.data
    state.updated.data = data
    state.updated.pending = false
  },

  [types.UPDATE_CONFIG_FAILED](state, payload) {
    let data = payload.data
    state.updated.pending = false
    state.updated.error = data
  },
};
