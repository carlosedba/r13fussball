/* ============
 * Mutation types for the config module
 * ============
 *
 * The mutation types that are available
 * on the config module.
 */

export const FIND_CONFIGS = 'FIND_CONFIGS'
export const FIND_CONFIGS_SUCCEEDED = 'FIND_CONFIGS_SUCCEEDED'
export const FIND_CONFIGS_FAILED = 'FIND_CONFIGS_FAILED'

export const FIND_CONFIG = 'FIND_CONFIG'
export const FIND_CONFIG_SUCCEEDED = 'FIND_CONFIG_SUCCEEDED'
export const FIND_CONFIG_FAILED = 'FIND_CONFIG_FAILED'

export const UPDATE_CONFIG = 'UPDATE_CONFIG'
export const UPDATE_CONFIG_SUCCEEDED = 'UPDATE_CONFIG_SUCCEEDED'
export const UPDATE_CONFIG_FAILED = 'UPDATE_CONFIG_FAILED'

