/* ============
 * State of the config module
 * ============
 *
 * The initial state of the config module.
 */

export default {
	list: 			{ data: [], pending: false, error: null },
	current: 		{ data: null, pending: false, error: null },
	updated: 		{ data: null, pending: false, error: null },
}
