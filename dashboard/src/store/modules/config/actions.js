/* ============
 * Actions for the config module
 * ============
 *
 * The actions that are available on the
 * config module.
 */
 
import Vue from 'vue'
import * as types from './mutation-types'

export default {
	[types.FIND_CONFIGS] ({ commit }) {
		const url = `/config`
		const request = Vue.$http.get(url)
		commit(types.FIND_CONFIGS, request)
	},

	[types.FIND_CONFIG] ({ commit }, { id }) {
		if (id) {
			const url = `/config/${id}`
			const request = Vue.$http.get(url)
			commit(types.FIND_CONFIG, request)
		}
	},

	[types.UPDATE_CONFIG] ({ commit }, { id, data }) {
		if (id && data) {
			const url = `/config/update/${id}`
			const request = Vue.$http.post(url, data)
			commit(types.UPDATE_CONFIG, request)
		}
	},
}
