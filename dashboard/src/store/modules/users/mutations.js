/* ============
 * Mutations for the user module
 * ============
 *
 * The mutations that are available on the
 * user module.
 */

import * as types from './mutation-types'

export default {
  /*
   * NET_FIND_USERS
   *
   */
  [types.NET_FIND_USERS](state) {
    state.list.pending = true
  },

  [types.NET_FIND_USERS_SUCCEEDED](state, payload) {
    let data = payload.data
    state.list.data = data
    state.list.pending = false
  },

  [types.NET_FIND_USERS_FAILED](state, payload) {
    let data = payload.data
    state.list.pending = false
    state.list.error = data
  },


  /*
   * NET_FIND_USER
   *
   */
  [types.NET_FIND_USER](state) {
    state.current.pending = true
  },

  [types.NET_FIND_USER_SUCCEEDED](state, payload) {
    let data = payload.data
    state.current.data = data
    state.current.pending = false
  },

  [types.NET_FIND_USER_FAILED](state, payload) {
    let data = payload.data
    state.current.pending = false
    state.current.error = data
  },


  /*
   * NET_CREATE_USER
   *
   */
  [types.NET_CREATE_USER](state) {
    state.created.pending = true
  },

  [types.NET_CREATE_USER_SUCCEEDED](state, payload) {
    let data = payload.data
    state.created.data = data
    state.created.pending = false
  },

  [types.NET_CREATE_USER_FAILED](state, payload) {
    let data = payload.data
    state.created.pending = false
    state.created.error = data
  },


  /*
   * NET_UPDATE_USER
   *
   */
  [types.NET_UPDATE_USER](state) {
    state.updated.pending = true
  },

  [types.NET_UPDATE_USER_SUCCEEDED](state, payload) {
    let data = payload.data
    state.updated.data = data
    state.updated.pending = false
  },

  [types.NET_UPDATE_USER_FAILED](state, payload) {
    let data = payload.data
    state.updated.pending = false
    state.updated.error = data
  },


  /*
   * NET_REMOVE_USER
   *
   */
  [types.NET_REMOVE_USER](state) {
    state.removed.pending = true
  },

  [types.NET_REMOVE_USER_SUCCEEDED](state, payload) {
    let data = payload.data
    state.removed.data = data
    state.removed.pending = false
  },

  [types.NET_REMOVE_USER_FAILED](state, payload) {
    let data = payload.data
    state.removed.pending = false
    state.removed.error = data
  },
};
