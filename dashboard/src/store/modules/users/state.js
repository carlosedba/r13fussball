/* ============
 * State of the user module
 * ============
 *
 * The initial state of the user module.
 */

export default {
	search: 		{ data: [], pending: false, error: null },
	list: 			{ data: [], pending: false, error: null },
	current: 		{ data: null, pending: false, error: null },
	created: 		{ data: null, pending: false, error: null },
	updated: 		{ data: null, pending: false, error: null },
	removed: 		{ data: null, pending: false, error: null },
}
