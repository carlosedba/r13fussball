/* ============
 * Actions for the user module
 * ============
 *
 * The actions that are available on the
 * user module.
 */
 
import Vue from 'vue'
import * as types from './mutation-types'

export default {
	[types.NET_FIND_USERS] ({ commit }) {
		const url = `/users`
		const request = Vue.$http.get(url)
		commit(types.NET_FIND_USERS, request)
	},

	[types.NET_FIND_USER] ({ commit }, { id }) {
		if (id) {
			const url = `/users/${id}`
			const request = Vue.$http.get(url)
			commit(types.NET_FIND_USER, request)
		}
	},

	[types.NET_CREATE_USER] ({ commit }, { data }) {
		if (data) {
			const url = `/users/create`
			const request = Vue.$http.post(url, data)
			commit(types.NET_CREATE_USER, request)
		}
	},

	[types.NET_UPDATE_USER] ({ commit }, { id, params }) {
		if (id && params) {
			const url = `/users/update/${id}`
			const request = Vue.$http.post(url, params)
			commit(types.NET_UPDATE_USER, request)
		}
	},

	[types.NET_REMOVE_USER] ({ commit }, { id }) {
		if (id) {
			const url = `/users/delete/${id}`
			const request = Vue.$http.delete(url)
			commit(types.NET_REMOVE_USER, request)
		}
	},
}
