/* ============
 * State of the component module
 * ============
 *
 * The initial state of the component module.
 */

export default {
	search: 		{ data: [], pending: false, error: null },
	list: 			{ data: [], pending: false, error: null },
	current: 		{ data: null, pending: false, error: null },
	created: 		{ data: null, pending: false, error: null },
	updated: 		{ data: null, pending: false, error: null },
	removed: 		{ data: null, pending: false, error: null },
}
