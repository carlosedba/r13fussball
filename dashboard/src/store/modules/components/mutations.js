/* ============
 * Mutations for the component module
 * ============
 *
 * The mutations that are available on the
 * component module.
 */

import * as types from './mutation-types'

export default {
  /*
   * FIND_COMPONENTS
   *
   */
  [types.FIND_COMPONENTS](state) {
    state.list.pending = true
  },

  [types.FIND_COMPONENTS_SUCCEEDED](state, payload) {
    let data = payload.data
    state.list.data = data
    state.list.pending = false
  },

  [types.FIND_COMPONENTS_FAILED](state, payload) {
    let data = payload.data
    state.list.pending = false
    state.list.error = data
  },


  /*
   * FIND_COMPONENT
   *
   */
  [types.FIND_COMPONENT](state) {
    state.current.pending = true
  },

  [types.FIND_COMPONENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.current.data = data
    state.current.pending = false
  },

  [types.FIND_COMPONENT_FAILED](state, payload) {
    let data = payload.data
    state.current.pending = false
    state.current.error = data
  },


  /*
   * CREATE_COMPONENT
   *
   */
  [types.CREATE_COMPONENT](state) {
    state.created.pending = true
  },

  [types.CREATE_COMPONENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.created.data = data
    state.created.pending = false
  },

  [types.CREATE_COMPONENT_FAILED](state, payload) {
    let data = payload.data
    state.created.pending = false
    state.created.error = data
  },


  /*
   * UPDATE_COMPONENT
   *
   */
  [types.UPDATE_COMPONENT](state) {
    state.updated.pending = true
  },

  [types.UPDATE_COMPONENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.updated.data = data
    state.updated.pending = false
  },

  [types.UPDATE_COMPONENT_FAILED](state, payload) {
    let data = payload.data
    state.updated.pending = false
    state.updated.error = data
  },


  /*
   * REMOVE_COMPONENT
   *
   */
  [types.REMOVE_COMPONENT](state) {
    state.removed.pending = true
  },

  [types.REMOVE_COMPONENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.removed.data = data
    state.removed.pending = false
  },

  [types.REMOVE_COMPONENT_FAILED](state, payload) {
    let data = payload.data
    state.removed.pending = false
    state.removed.error = data
  },
};
