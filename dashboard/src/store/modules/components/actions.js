/* ============
 * Actions for the component module
 * ============
 *
 * The actions that are available on the
 * component module.
 */
 
import Vue from 'vue'
import * as types from './mutation-types'

export default {
	[types.FIND_COMPONENTS] ({ commit }) {
		const url = `/components`
		const request = Vue.$http.get(url)
		commit(types.FIND_COMPONENTS, request)
	},

	[types.FIND_COMPONENT] ({ commit }, { id }) {
		if (id) {
			const url = `/components/${id}`
			const request = Vue.$http.get(url)
			commit(types.FIND_COMPONENT, request)
		}
	},

	[types.CREATE_COMPONENT] ({ commit }, { data }) {
		if (data) {
			const url = `/components/create`
			const request = Vue.$http.post(url, data)
			commit(types.CREATE_COMPONENT, request)
		}
	},

	[types.UPDATE_COMPONENT] ({ commit }, { id, data }) {
		if (id && data) {
			const url = `/components/update/${id}`
			const request = Vue.$http.post(url, data)
			commit(types.UPDATE_COMPONENT, request)
		}
	},

	[types.REMOVE_COMPONENT] ({ commit }, { id }) {
		if (id) {
			const url = `/components/delete/${id}`
			const request = Vue.$http.delete(url)
			commit(types.REMOVE_COMPONENT, request)
		}
	},
}
