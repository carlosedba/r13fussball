/* ============
 * Actions for the content module
 * ============
 *
 * The actions that are available on the
 * content module.
 */
 
import Vue from 'vue'
import * as types from './mutation-types'
import queryString from 'query-string'

export default {
	[types.NET_FIND_CONTENTS] ({ commit }, params) {
		let url = `/content`

		if (params) {
			params = queryString.stringify(params)
			url = `/content?${params}`
		}

		const request = Vue.$http.get(url)
		commit(types.NET_FIND_CONTENTS, request)
	},

	[types.NET_FIND_CONTENT] ({ commit }, { id }) {
		if (id) {
			const url = `/content/${id}`
			const request = Vue.$http.get(url)
			commit(types.NET_FIND_CONTENT, request)
		}
	},

	[types.NET_CREATE_CONTENT] ({ commit }, { data }) {
		if (data) {
			const url = `/content/create`
			const request = Vue.$http.post(url, data)
			commit(types.NET_CREATE_CONTENT, request)
		}
	},

	[types.NET_UPDATE_CONTENT] ({ commit }, { id, params }) {
		if (id && params) {
			const url = `/content/update/${id}`
			const request = Vue.$http.post(url, params)
			commit(types.NET_UPDATE_CONTENT, request)
		}
	},

	[types.NET_REMOVE_CONTENT] ({ commit }, { id }) {
		if (id) {
			const url = `/content/delete/${id}`
			const request = Vue.$http.delete(url)
			commit(types.NET_REMOVE_CONTENT, request)
		}
	},

	[types.UPDATE_CONTENT_ORDER] ({ commit }, { data }) {
		if (data) {
			commit(types.UPDATE_CONTENT_ORDER, data)
		}
	},
}
