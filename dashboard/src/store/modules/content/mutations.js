/* ============
 * Mutations for the content module
 * ============
 *
 * The mutations that are available on the
 * content module.
 */

import * as types from './mutation-types'

export default {
  /*
   * UPDATE_CONTENT_ORDER
   *
   */
  [types.UPDATE_CONTENT_ORDER](state, payload) {
    state.list.data = payload.map((el, i) => {
      el.order = i + 1
      return el
    })
  },

  /*
   * FIND_CONTENTS
   *
   */
  [types.NET_FIND_CONTENTS](state) {
    state.list.pending = true
  },

  [types.NET_FIND_CONTENTS_SUCCEEDED](state, payload) {
    let data = payload.data
    state.list.data = data
    state.list.pending = false
  },

  [types.NET_FIND_CONTENTS_FAILED](state, payload) {
    let data = payload.data
    state.list.pending = false
    state.list.error = data
  },


  /*
   * FIND_CONTENT
   *
   */
  [types.NET_FIND_CONTENT](state) {
    state.current.pending = true
  },

  [types.NET_FIND_CONTENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.current.data = data
    state.current.pending = false
  },

  [types.NET_FIND_CONTENT_FAILED](state, payload) {
    let data = payload.data
    state.current.pending = false
    state.current.error = data
  },


  /*
   * CREATE_CONTENT
   *
   */
  [types.NET_CREATE_CONTENT](state) {
    state.created.pending = true
  },

  [types.NET_CREATE_CONTENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.created.data = data
    state.created.pending = false
  },

  [types.NET_CREATE_CONTENT_FAILED](state, payload) {
    let data = payload.data
    state.created.pending = false
    state.created.error = data
  },


  /*
   * UPDATE_CONTENT
   *
   */
  [types.NET_UPDATE_CONTENT](state) {
    state.updated.pending = true
  },

  [types.NET_UPDATE_CONTENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.updated.data = data
    state.updated.pending = false
  },

  [types.NET_UPDATE_CONTENT_FAILED](state, payload) {
    let data = payload.data
    state.updated.pending = false
    state.updated.error = data
  },


  /*
   * REMOVE_CONTENT
   *
   */
  [types.NET_REMOVE_CONTENT](state) {
    state.removed.pending = true
  },

  [types.NET_REMOVE_CONTENT_SUCCEEDED](state, payload) {
    let data = payload.data
    state.removed.data = data
    state.removed.pending = false
  },

  [types.NET_REMOVE_CONTENT_FAILED](state, payload) {
    let data = payload.data
    state.removed.pending = false
    state.removed.error = data
  },
};
