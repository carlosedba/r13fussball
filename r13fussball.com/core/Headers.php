<?php
define('NAMESPACE', 'Rise');

define('ROOT_PATH', 		dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('CACHE_PATH', 		dirname(__DIR__) . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR);
define('DATA_PATH', 		dirname(__DIR__) . DIRECTORY_SEPARATOR . 'public_html/data' . DIRECTORY_SEPARATOR);
define('USER_DATA_PATH', 	dirname(__DIR__) . DIRECTORY_SEPARATOR . 'public_html/data/user_data' . DIRECTORY_SEPARATOR);
define('CONTENT_DATA_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'public_html/data/content_data' . DIRECTORY_SEPARATOR);
define('POST_DATA_PATH', 	dirname(__DIR__) . DIRECTORY_SEPARATOR . 'public_html/data/post_data' . DIRECTORY_SEPARATOR);
define('PAGE_DATA_PATH', 	dirname(__DIR__) . DIRECTORY_SEPARATOR . 'public_html/data/page_data' . DIRECTORY_SEPARATOR);
define('LOGS_PATH', 		dirname(__DIR__) . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR);
define('PUBLIC_PATH', 		dirname(__DIR__) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR);
define('THEMES_PATH', 		dirname(__DIR__) . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR);
define('VENDOR_PATH', 		dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR);

define('CORE_PATH',			__DIR__ . DIRECTORY_SEPARATOR);
define('CONTROLLERS_PATH',	__DIR__ . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR);
define('DIRECTIVES_PATH', 	__DIR__ . DIRECTORY_SEPARATOR . 'Directives' . DIRECTORY_SEPARATOR);
define('MIDDLEWARES_PATH', 	__DIR__ . DIRECTORY_SEPARATOR . 'Middlewares' . DIRECTORY_SEPARATOR);
define('MODELS_PATH', 		__DIR__ . DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR);
define('TEMPLATES_PATH', 	__DIR__ . DIRECTORY_SEPARATOR . 'Templates' . DIRECTORY_SEPARATOR);
define('UTILS_PATH', 		__DIR__ . DIRECTORY_SEPARATOR . 'Utils' . DIRECTORY_SEPARATOR);
define('VIEWS_PATH', 		__DIR__ . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR);

define('PRIVATE_KEY', __DIR__ . DIRECTORY_SEPARATOR . 'Keys/private.pem');
define('PUBLIC_KEY', __DIR__ . DIRECTORY_SEPARATOR . 'Keys/public.pub');
?>

