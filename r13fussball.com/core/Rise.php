<?php
namespace Rise;

require_once('Bootstrap.php');

use \Model;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Utils\IdGenerator;
use Rise\Auth\JWT;
use Rise\Models\User;

$container = new Container($config);
$net = new Netcore($container);

$net->add(new JWT([
	"secure" => false,
	"signature" => "RSA",
	"algorithm" => "SHA256",
	"key" => PUBLIC_KEY,
	"path" => ["/admin", "/api", "/wow"],
	"ignore" => ["/api/token"],
	"callback" => function ($request, $response, $args) use ($container) {
		$container->set('token', $args['token']);
		return $response;
	},
	"error" => function ($request, $response, $args) {
		var_dump($args);
		//return $response->withRedirect($request->getUri()->getBasePath() . '/login');
	}
]));

/*
$net->options('/{routes:.+}', function ($request, $response, $args) {
	return $response;
});

$net->add(function ($req, $res, $next) {
	$response = $next($req, $res);
	return $response
			->withHeader('Access-Control-Allow-Origin', 'http://r13fussball.com.br')
			->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
			->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});
*/

$net->group('/api', function () {
	$this->delete('/users/delete/{id}[/]', 			constant('NAMESPACE') . '\Api\Users:delete');
	$this->post('/users/update/{id}[/]', 			constant('NAMESPACE') . '\Api\Users:update');
	$this->post('/users/create[/]', 				constant('NAMESPACE') . '\Api\Users:create');	
	$this->get('/users/limit[/]',					constant('NAMESPACE') . '\Api\Users:limit');
	$this->get('/users/email/{email}[/]', 			constant('NAMESPACE') . '\Api\Users:findOne');
	$this->get('/users/{id}[/]', 					constant('NAMESPACE') . '\Api\Users:findOneById');
	$this->get('/users[/]', 						constant('NAMESPACE') . '\Api\Users:findAll');

	$this->delete('/config/delete/{id}[/]', 		constant('NAMESPACE') . '\Api\Config:delete');
	$this->post('/config/update/{id}[/]', 			constant('NAMESPACE') . '\Api\Config:update');
	$this->post('/config/create[/]', 				constant('NAMESPACE') . '\Api\Config:create');	
	$this->get('/config/{id}[/]', 					constant('NAMESPACE') . '\Api\Config:findOneById');
	$this->get('/config[/]', 						constant('NAMESPACE') . '\Api\Config:findAll');

	$this->delete('/components/delete/{id}[/]', 	constant('NAMESPACE') . '\Api\Components:delete');
	$this->post('/components/update/{id}[/]', 		constant('NAMESPACE') . '\Api\Components:update');
	$this->post('/components/create[/]', 			constant('NAMESPACE') . '\Api\Components:create');	
	$this->get('/components/{id}[/]', 				constant('NAMESPACE') . '\Api\Components:findOneById');
	$this->get('/components[/]', 					constant('NAMESPACE') . '\Api\Components:findAll');

	$this->delete('/content/delete/{id}[/]', 	constant('NAMESPACE') . '\Api\Content:delete');
	$this->post('/content/update/{id}[/]', 		constant('NAMESPACE') . '\Api\Content:update');
	$this->post('/content/create[/]', 			constant('NAMESPACE') . '\Api\Content:create');	
	$this->get('/content/{id}[/]', 				constant('NAMESPACE') . '\Api\Content:findOneById');
	$this->get('/content[/]', 					constant('NAMESPACE') . '\Api\Content:findAll');
});

$net->post('/api/token', function (Request $request, Response $response, $args) {
	$data = $request->getParsedBody();
	$email = $data['email'];
	$password = $data['password'];
	$factory = Model::factory('User');

	if ($user = $factory->where('email', $email)->findOne()) {
		if ($user->password == hash('sha256', $password)) {
			$token = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, ['id' => $user->id])->__toString();
			$json = json_encode(array('token' => $token));
			$response->getBody()->write($json);

			return $response;	
		} else {
			$json = json_encode(array(
				'error' => '1001',
				'message' => 'Invalid password.'
			));
			$response->getBody()->write($json);
		}
	} else {
		$json = json_encode(array(
			'error' => '1002',
			'message' => 'Email not found.'
		));
		$response->getBody()->write($json);
	}

	return $response;
});

$net->get('/', function (Request $request, Response $response, $args) {
	//$token = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, array("wow" => "teste", "lalala" => "hehehe"));
	//$isLegit = JWT::verify('RSA', 'SHA256', PUBLIC_KEY, $token);

	$selected = array();
	$configs = Model::factory('Config')->findArray();

	foreach ($configs as $config) {
		if ($config['autoload'] === '1') {
			$selected = array_merge($selected, array($config['name'] => $config['value']));
		}
	}

	$component = array();

	$content = Model::factory('Content')
				->where('id_component', '1')
				->orderByAsc('order')
				->findArray();
	$component = array_merge($component, array('Menu' => $content));

	$content = Model::factory('Content')
				->where('id_component', '2')
				->orderByAsc('order')
				->findArray();
	$component = array_merge($component, array('Slider' => $content));

	$content = Model::factory('Content')
				->where('id_component', '3')
				->orderByAsc('order')
				->findArray();
	$component = array_merge($component, array('MissaoVisao' => $content));

	$content = Model::factory('Content')
				->where('id_component', '4')
				->orderByAsc('order')
				->findArray();	
	$component = array_merge($component, array('CarouselServicos' => $content));

	$content = Model::factory('Content')
				->where('id_component', '5')
				->orderByAsc('order')
				->findArray();	
	$component = array_merge($component, array('CarouselPortfolio' => $content));


	$content = Model::factory('Content')
				->where('id_component', '6')
				->orderByAsc('order')
				->findArray();	
	$component = array_merge($component, array('CarouselClientesParceiros' => $content));


	$content = Model::factory('Content')
				->where('id_component', '7')
				->orderByAsc('order')
				->findArray();	
	$component = array_merge($component, array('FaleConosco' => $content));

	return $this->rise->render($response, 'lalala', array(
		'theme' => $selected['site_url'] . 'themes/' . $selected['theme'],
		'config' => $selected,
		'component' => $component,
	));
});

$net->get('/login', function ($request, $response, $args) {
	$response->getBody()->write('something wrong happened :o');
	return $response;
});

$net->get('/id', function (Request $request, Response $response, $args) {
	$response->getBody()->write(IdGenerator::uniqueId(8));
	return $response;
});

$net->run();
?>
