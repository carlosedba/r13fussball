const Promise     = require('bluebird')
const fs          = Promise.promisifyAll(require('fs'))
const path        = require('path')
const gulp        = require('gulp')
const rename      = require('gulp-rename')
const htmlmin     = require('gulp-htmlmin')
const hb          = require('gulp-hb')
const handlebars  = require('handlebars')
const layouts     = require('handlebars-layouts')
const vfs         = require('vinyl-fs')

const LIBRARY_PATH = path.resolve(__dirname, '../library/dist')

async function getLibrary() {
  let arr = []
  let library = { stylesheets: [], scripts: [] }
  const dirContent = await fs.readdirAsync(LIBRARY_PATH).catch(console.log)

  for (let child of dirContent) {
    const childPath = path.join(LIBRARY_PATH, child)
    const parsedChildPath = path.parse(childPath)
    const childStat = await fs.statAsync(childPath).catch(console.log)

    if (parsedChildPath.ext === '.css') {
      library.stylesheets.push(`{site/url}resources/css/${parsedChildPath.name}.css`)
    }

    if (parsedChildPath.ext === '.js') {
      library.scripts.push(`{site/url}resources/js/${parsedChildPath.name}.js`)
    }
  }

  arr = library.scripts

  arr.forEach((el, i) => {
    if (el.match(/(\/manifest)/)) { library.scripts.splice(0, 0, library.scripts.splice(i, 1)[0]) }
    if (el.match(/(\/vendor)/)) { library.scripts.splice(1, 0, library.scripts.splice(i, 1)[0]) }
    if (el.match(/(\/lib)/)) { library.scripts.splice(2, 0, library.scripts.splice(i, 1)[0]) }
  })

  return library
}

gulp.task('generate-templates', async () => {
  const library = await getLibrary()
  const stream = hb()
    .partials('src/pages/layouts/**/*.{hbs,js}')
    .partials('src/pages/partials/**/*.{hbs,js}')
    .helpers(layouts)
    .data({
      stylesheets: library.stylesheets,
      scripts: library.scripts.concat([
        '{site/url}resources/js/SistemaFiepFW.js',
      ])
    })

  return gulp.src('src/pages/*.hbs')
    .pipe(stream)
    .pipe(rename({ extname: '.xsl' }))
    .pipe(gulp.dest('dist/pages'))
})

gulp.task('minify-templates', () => {
  return gulp.src('dist/pages/*.xsl')
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(gulp.dest('dist/pages'))
})

gulp.task('build', ['generate-templates'])