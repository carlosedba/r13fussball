const path = require('path')
const webpack = require('webpack')


module.exports = function (env) {
	return {
		target: 'web',

		entry: {
			'lib': path.resolve(__dirname, 'src/index.js')
		},

		output: {
			filename: '[name].js',
			path: path.resolve(__dirname, 'dist'),
			publicPath: './',
			sourceMapFilename: '[name].map'
		},

  		resolve: {
  			alias: {
  				'@': path.resolve(__dirname, 'src')
  			}
  		},

		module: {
			rules: [
				{
					test: /\.(js|jsx)?$/,
					exclude: [
						path.resolve(__dirname, 'node_modules')
					],
					loader: 'babel-loader'
				},
				{
					test: /\.(js)?$/,
					exclude: [
						path.resolve(__dirname, 'node_modules')
					],
					include: [
						path.resolve(__dirname, 'src/vendor')
					],
					loader: 'script-loader'
				},
				{
					test:   /\.css?$/,
					use: [
						'style-loader',
						'css-loader'
					]
				},
				{
					test:   /\.scss?$/,
					use: [
						'style-loader',
						'css-loader',
						'sass-loader'
					]
				},
				{
					test: /\.(png|jpg|jpeg)?$/,
					use: { loader: 'url-loader', options: { limit: 100000 } }
				},
				{ 
					test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					use: { loader: 'url-loader', options: { limit: 100000, mimetype: 'application/font-woff' } }
				},
				{
					test: /\.(swoff|woff|woff2|eot|ttf|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					loader: 'file-loader'
				},
				{ 
					test: /\.svg$/,
					loader: 'svg-inline-loader'
				}
			]
		},

		plugins: [
			new webpack.DefinePlugin({
      			'process.env': env
			}),
		]
	}
}

