import Event from '@/utils/Event'
import Generic from '@/utils/Generic'

export default class Hero {
	constructor() {
		console.log('log > SistemaFiep > Hero - initialized!')

		this.windowWidth = window.innerWidth
		
		this.player = null

		this.Event = new Event()
		this.Event.addHandler('handlePlayClick', this.handlePlayClick, this)
		this.Event.addHandler('handleOverlayClose', this.handleOverlayClose, this)
		this.Event.addHandler('initYoutubePlayer', this.initYoutubePlayer, this)

		document.addEventListener('DOMContentLoaded', this.render.bind(this))
		window.addEventListener('resize', this.setPlayerSize.bind(this))
	}

	initYoutubePlayer() {
		console.log('log > SistemaFiep > Hero - initializing YouTube Player')

		this.player = new YT.Player('player', {
			width: '640',
			height: '360',
			videoId: 'ccFDotDGTY0',
			playerVars: {
				rel: 0,
				showinfo: 0,
			},
			events: {
				onReady: function (event) { this.onPlayerReady(event) }.bind(Hero.prototype),
				onStateChange: function (event) { this.onPlayerStateChange(event) }.bind(Hero.prototype),
			}	
		})

		this.setPlayerSize()
	}

	onPlayerReady(event) {
		console.log('log > SistemaFiep > Hero - YouTube Player Event: ready')
	}

	onPlayerStateChange(event) {
		console.log('log > SistemaFiep > Hero - YouTube Player Event: stateChange')
	}

	handlePlayClick(event) {
		let overlay = document.querySelector('.hero-frame')

		overlay.classList.add('open')

		this.player.playVideo()
	}

	handleOverlayClose(event) {
		let overlay = document.querySelector('.hero-frame')

		overlay.classList.remove('open')

		this.player.stopVideo()
	}

	setPlayerSize(event) {
		if (this.player !== null) {
			if (Modernizr.mq('(min-width: 256px)')) {
				this.player.setSize('256', '144')
			}

			if (Modernizr.mq('(min-width: 384px)')) {
				this.player.setSize('384', '216')
			}

			if (Modernizr.mq('(min-width: 512px)')) {
				this.player.setSize('512', '288')
			}

			if (Modernizr.mq('(min-width: 640px)')) {
				this.player.setSize('640', '360')
			}

			if (Modernizr.mq('(min-width: 768px)')) {
				this.player.setSize('768', '432')
			}
		}
	}

	render(event) {
		console.log('log > SistemaFiep > Hero - render called!')

		if (Generic.shouldRender(this.windowWidth, event)) {
			console.log('log > SistemaFiep > Hero - render approved!')

			const player = document.querySelector('#player')

			if (player !== null && player !== undefined) {
				let play = document.querySelector('.hero-play-button')
				let overlay = document.querySelector('.hero-frame')
				let close = document.querySelector('.hero-frame .close')

				this.Event.addTo(play, 'click', 'handlePlayClick')
				this.Event.addTo(close, 'click', 'handleOverlayClose')
				this.Event.addTo(overlay, 'click', 'handleOverlayClose')
				this.Event.addTo(document, 'YouTubeIframeAPIReady', 'initYoutubePlayer')
			}
		}
	}
}