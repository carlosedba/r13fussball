import Event from '@/utils/Event'

export default class SistemaFiep {
	constructor() {
		console.log('log > SistemaFiep - initialized!')

		this.Event = new Event()
		//this.Event.addHandler('handleMobileClick', this.handleMobileClick, this)

		document.addEventListener('DOMContentLoaded', this.render.bind(this))
		window.addEventListener('resize', this.render.bind(this))
	}

	featuredSolutions() {
	    let solutions = document.querySelectorAll('.featured-solution')

	    ;[].forEach.call(solutions, function (el, i) {
	        let circles = el.querySelectorAll('.solution-ball')
	        let increase = Math.PI * 2 / circles.length
	        let x = 0, y = 0, angle = 0

	        for (var i = 0; i < circles.length; i++) {
	            let circle = circles[i]
	            let radius = el.offsetWidth / 2
	            console.log(radius)
	            x = radius * Math.cos(angle) + (radius / 1.5)
	            y = radius * Math.sin(angle) + (radius / 1.5)
	            circle.style.position = 'absolute'
	            circle.style.left = x + 'px'
	            circle.style.top = y + 'px'
	            //let rot = 90 + (i * (360 / circles.length))  
	            angle += increase

	            circle.addEventListener('click', function (e) {
	                e.preventDefault()
	                let url = circle.dataset.url
	                window.open(url, '_blank')
	            })
	        }
	    })
	}

	render() {
		this.featuredSolutions()
	}
}