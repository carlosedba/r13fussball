import Event from '@/utils/Event'
import Loading from '@/lib/Loading'

export default class Search {
	constructor() {
		console.log('log > SistemaFiep > Search - initialized!')

		this.windowWidth = window.innerWidth

		this.ready = true
		this.next = null

		this.Event = new Event()
		this.Event.addHandler('handleEnter', this.handleEnter, this)
		this.Event.addHandler('handleSubmit', this.handleSubmit, this)
		this.Event.addHandler('handleScroll', this.handleScroll, this)

		document.addEventListener('DOMContentLoaded', this.render.bind(this))
	}

	waitForAxios() {
		return new Promise(function (resolve, reject) {
			let loop = setInterval(function () {
				if (window.axios !== undefined && window.axios !== null) {
					clearInterval(loop)
					resolve()
				}
			}, 100)
		})
	}

	clearResults() {
		const searchList = document.querySelector('.search-list')

		while (searchList.children.length !== 4) {
			if (searchList.lastElementChild.classList.contains('search-result')) {
				searchList.removeChild(searchList.lastElementChild)
			}
		}
	}

	loadResults(query) {	
		const searchList = document.querySelector('.search-list')
		const searchbar = document.querySelector('.searchbar input')

		console.log('ready', this.ready)

		if (searchbar.value && this.ready) {
			var params = {
				method: 'GET',
				url: 'https://www.googleapis.com/customsearch/v1',
				params: {
					key: 'AIzaSyAHzTpfwnddEZ1SVqVx1CRKDuEKFskGQRY',
					cx: '009532474545508146287:juaqa93rpfs',
					cr: 'pt-BR',
					hl: 'pt-BR',
					num: 10,
					safe: 'medium',
					siteSearch: 'www.sistemafiep.org.br',
					siteSearchFilter: 'i',
				}
			}

			if (this.next === null || query) {
				console.log('!next')
				params = Object.assign({}, params)
				params.params.q = query
			} else {
				console.log('next')
				params = Object.assign({}, params)
				params.params.q = this.next.searchTerms
				params.params.start = this.next.startIndex
			}

			if (this.next === null || query) {
				searchList.classList.remove('no-query')
				searchList.classList.remove('no-results')
				searchList.classList.remove('unavailable')
				searchList.classList.add('waiting')
				Loading.activate()
			}

			if (query) this.clearResults()

			axios(params)
			.then(function (response) {
				if (this.next === null || query) Loading.deactivate()
				let items = response.data.items

				if (items !== undefined && items.length) {
					searchList.classList.remove('waiting')
					items.forEach(this.renderResult, this)
				} else {
					searchList.classList.remove('waiting')
					if (this.next === null || query) searchList.classList.add('no-results')
				}

				if (response.data.queries.nextPage !== undefined && response.data.queries.nextPage.length) {
					this.next = response.data.queries.nextPage[0]
				} else {
					this.next = null
				}
			}.bind(this))
			.catch(function (response) {
				setTimeout(function () {
					Loading.deactivate()
					
					searchList.classList.remove('waiting')
					if (this.next === null || query) searchList.classList.add('unavailable')
				}, 2500)
			})

			this.ready = false
			setTimeout(function () { this.ready = true }.bind(this), 1500)
		}
	}

	handleEnter(event) {
		if (event.keyCode === 13) this.handleSubmit(event)
	}

	handleSubmit(event) {
		event.preventDefault()
		const searchbar = document.querySelector('.searchbar input')
		const params = new URLSearchParams(location.search)
		const query = searchbar.value || params.get('query') || ''
		searchbar.value = query

		if (query.length) {
			this.loadResults(query)
			history.pushState({ query: query }, 'Busca: ' + query, location.origin + '/busca?query=' + query)
		}
	}

	handleScroll(event) {
		const searchList = document.querySelector('.search-list')

		if (window.scrollY > (searchList.getBoundingClientRect().top + searchList.offsetHeight)) {
			this.loadResults()
		}
	}

	renderResult(el, i, arr) {
		const searchList = document.querySelector('.search-list')

		const searchResult = document.createElement('a')
		searchResult.setAttribute('class', 'search-result')
		searchResult.href = el.link
		
		const searchResultWrapper = document.createElement('div')
		searchResultWrapper.setAttribute('class', 'search-result-wrapper')
		
		const searchResultLeft = document.createElement('div')
		searchResultLeft.setAttribute('class', 'search-result-left')
		
		const searchResultTitle = document.createElement('a')
		searchResultTitle.setAttribute('class', 'search-result-title')
		searchResultTitle.href = el.link
		searchResultTitle.innerHTML = el.title
		
		const searchResultSnippet = document.createElement('p')
		searchResultSnippet.setAttribute('class', 'search-result-snippet')
		searchResultSnippet.innerHTML = el.snippet
		
		const searchResultRight = document.createElement('div')
		searchResultRight.setAttribute('class', 'search-result-right')
		
		const searchResultArrow = document.createElement('div')
		searchResultArrow.setAttribute('class', 'search-result-arrow')
		searchResultArrow.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 59.4 59.4"><path d="M20.6 0l-.9 1.4 18.2 28.3L19.7 58l.9 1.4 19.1-29.7"/></svg>'

		searchResult.appendChild(searchResultWrapper)
		searchResultWrapper.appendChild(searchResultLeft)
		searchResultWrapper.appendChild(searchResultRight)
		searchResultLeft.appendChild(searchResultTitle)
		searchResultLeft.appendChild(searchResultSnippet)
		searchResultRight.appendChild(searchResultArrow)

		searchList.appendChild(searchResult)
	}

	render(event) {
		console.log('log > SistemaFiep > Search - render called!')

		const search = document.querySelector('.searchbar form')

		if (search !== null && search !== undefined) {
			const btn = document.querySelector('.searchbar button')

			this.Event.addTo(search, 'submit', 'handleSubmit')
			this.Event.addTo(btn, 'click', 'handleSubmit')
			this.Event.addTo(search, 'keypress', 'handleEnter')
			this.Event.addTo(document, 'scroll', 'handleScroll')

			this.waitForAxios().then(this.handleSubmit.bind(this, event))
		}
	}
}