// ==========================
// CSS
// ==========================

// Web vendor
import '@/vendor/normalize/normalize.css'
import '@/vendor/hamburgers/hamburgers.min.css'
import '@/vendor/css-loaders/css/load8.css'

// Application
import '@/assets/scss/main.scss'


// ==========================
// JS
// ==========================

// Polyfills
import 'babel-polyfill'
import '@/vendor/classlist/classList.min.js'
import '@/vendor/iterators-polyfill/polyfill.min.js'
import '@/vendor/urlsearchparams/URLSearchParams.js'
import '@/vendor/event/EventListener.js'
import '@/vendor/customevent/polyfill.js'

// Web vendor
import '@/vendor/modernizr/modernizr-custom.js'
import '@/vendor/gsap/TweenMax.min.js'
import '@/vendor/gsap/plugins/CSSRulePlugin.min.js'

// NPM vendor
import 'axios'

// Application
import FooterMenu from '@/lib/FooterMenu'
import Form from '@/lib/Form'
import Hero from '@/lib/Hero'
import Loading from '@/lib/Loading'
import LP from '@/lib/LP'
import Menu from '@/lib/Menu'
import Search from '@/lib/Search'
import SistemaFiep from '@/lib/SistemaFiep'
import Tabs from '@/lib/Tabs'
import WebP from '@/lib/WebP'


// ==========================
// Initialization
// ==========================

new FooterMenu()
new Form()
new Hero()
new Loading()
new LP()
new Menu()
new Search()
new SistemaFiep()
new Tabs()
new WebP()


