export default class Generic {
	static isInt(num) {
		if (num % 1 === 0) {
			return true
		} else if (num % 1 !== 0) {
			return false
		}
	}

	static extend(a, b) {
		for (var i in b) {
			a[i] = b[i]
		}
	}

	static getQueryParam(param) {
		let query = window.location.search.substring(1)
		let vars = query.split('&')

		for (let i = 0; i < vars.length; i++) {
			let pair = vars[i].split('=')

			if (decodeURIComponent(pair[0]) == param) {
				return decodeURIComponent(pair[1])
			}
		}
	}

	static shouldRender(storedWidth, event) {
		if (event.type === 'DOMContentLoaded' || (event.type === 'resize' && window.innerWidth !== storedWidth)) {
			return true
		} else {
			return false
		}
	}
}