var heeSlider = function (element, options) {
	this.dom = {
		slider: element,
		slides: element.querySelector('.slides'),
		slide: element.querySelectorAll('.slides > .slide'),
		controlPrev: element.querySelector('.controls > .prev'),
		controlNext: element.querySelector('.controls > .next'),
		counter: element.querySelector('.counter'),
		slideball: element.querySelectorAll('.counter > .slideball')
	}

	this.indexes = {
		total: null,
		prev: null,
		current: null,
		next: null
	}

	this.states = ['prev', 'current', 'next']

	this.setIndexes()
	this.setSlides()
	this.setControls()
	//this.watchIndexes()
}

heeSlider.prototype.setIndexes = function () {
	this.indexes.total = this.dom.slide.length

	if (this.indexes.prev === null && this.indexes.current === null && this.indexes.next === null) {
		var firstSlide = Math.floor(this.indexes.total / 2)
		var previousSlide = firstSlide - 1
		var nextSlide = firstSlide + 1

		this.indexes.prev = previousSlide
		this.indexes.current = firstSlide
		this.indexes.next = nextSlide
	}

	console.log(this.indexes)

	return this
}

heeSlider.prototype.updateIndexes = function (index, callback) {
	var slidesLength = this.dom.slide.length

	this.indexes.current = index

	if (this.indexes.current > 0) {
		this.indexes.prev = this.indexes.current - 1
	} else {
		this.indexes.prev = null
	}

	if (this.indexes.current < slidesLength - 1) {
		this.indexes.next = this.indexes.current + 1
	} else {
		this.indexes.next = null
	}

	if (callback) callback()

	console.log(this.indexes)

	return this;
}

heeSlider.prototype.clearState = function (slide) {
	this.dom.slide[slide].classList.remove('hidden')
	this.dom.slide[slide].style.transform = ''

	return this
}

heeSlider.prototype.setSlides = function () {
	;[].forEach.call(this.dom.slide, function (slide, ind, arr) {
		switch (ind) {
			case this.indexes.prev:
				slide.style.display = 'flex'
				slide.style.transform = 'scale(0.95) translateX(-104%)'
				break
			case this.indexes.current:
				slide.style.display = 'flex'
				slide.style.transform = 'scale(1) translateX(0)'
				break
			case this.indexes.next:
				slide.style.display = 'flex'
				slide.style.transform = 'scale(0.95) translateX(104%)'
				break
			default:
				slide.classList.add('hidden')
		}
	}, this)

	/*
	this.dom.slide[this.indexes.prev].style.display = 'flex'
	this.dom.slide[this.indexes.prev].style.transform = 'scale(0.95) translateX(-104%)'

	//this.dom.slide[this.indexes.current].classList.add('current')
	this.dom.slide[this.indexes.current].style.display = 'flex'
	this.dom.slide[this.indexes.current].style.transform = 'scale(1) translateX(0)'

	//this.dom.slide[this.indexes.next].classList.add('next')
	this.dom.slide[this.indexes.next].style.display = 'flex'
	this.dom.slide[this.indexes.next].style.transform = 'scale(0.95) translateX(104%)'
	*/

	return this
}

heeSlider.prototype.updateSlide = function () {
	if (this.indexes.current > 0) {
		this.clearState(this.indexes.prev)
		Velocity.animate(
			this.dom.slide[this.indexes.prev],
			{
				translateX: "-100%",
				scale: "0.95"
			},
			{
				delay: 0,
				duration: 360
			},
			'easeInOutQuint'
		)
	}

	this.clearState(this.indexes.current)
	setTimeout(function() {
		Velocity.animate(
			this.dom.slide[this.indexes.current],
			{
				translateX: "0",
				scale: "1"
			},
			{
				delay: 0,
				duration: 360,
				queue: false
			},
			'easeInOutQuint'
		)
	}.bind(this), 200)

	if (this.indexes.current < this.indexes.total - 1) {
		this.clearState(this.indexes.next)
		setTimeout(function() {
			Velocity.animate(
				this.dom.slide[this.indexes.next],
				{
					translateX: "100%",
					scale: "0.95"
				},
				{
					delay: 0,
					duration: 360,
					queue: false
				},
				'easeInOutQuint'
			)
		}.bind(this), 200)
	}

	;[].forEach.call(this.dom.slide, function (slide, ind, arr) {
		if (
			ind !== this.indexes.prev &&
			ind !== this.indexes.current &&
			ind !== this.indexes.next
		) {
			slide.classList.add('hidden')
			slide.style.transform = ''
		}
	}, this)

	return this
}

heeSlider.prototype.setControls = function () {
	this.dom.controlPrev.addEventListener('click', this.prev.bind(this))
	this.dom.controlNext.addEventListener('click', this.next.bind(this))

	return this
}

heeSlider.prototype.prev = function () {
	console.log('prev')
	if (this.indexes.prev !== null) {
		this.updateIndexes(this.indexes.prev)
		this.updateSlide()
	}

	return this
}

heeSlider.prototype.next = function () {
	console.log('next')
	if (this.indexes.next !== null) {
		this.updateIndexes(this.indexes.next)
		this.updateSlide()
	}

	return this
}

heeSlider.prototype.watchIndexes = function () {
	this.indexes.watch('current', function (id, prevSlide, currentSlide) {
		console.log('o.' + id + ' changed from ' + prevSlide + ' to ' + currentSlide)

		//this.clearStates()
		this.updateSlide(prevSlide, currentSlide)

		if (prevSlide > currentSlide) {
			console.log('prev')
		} else {
			console.log('next')
		}

		return currentSlide
	}.bind(this))

	return this
}




