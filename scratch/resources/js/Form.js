const R13 = window.R13 = function () {
	console.log('log > R13 - initialized!')
}

const Form = R13.prototype.Form = function () {
	console.log('log > R13 > Form - initialized!')

	this.Event = new window.XEvent()
	this.Event.addHandler('handleSubmit', this.handleSubmit, this)

	document.addEventListener('DOMContentLoaded', this.render.bind(this))
	//window.addEventListener('resize', this.render.bind(this))
}

Form.prototype.countEntries = function (params) {
	let entries = params.entries()
	let i = 0

	while (entries.next().value !== undefined) {
		i++
	}

	return i
}

Form.prototype.validate = function (form, params) {
	let entriesNumber = this.countEntries(params)
	let entries = params.entries()

	for (let i = 0; i < entriesNumber; i++) {
		let pair = entries.next().value
		let input = form.querySelector('[name="' + pair[0] + '"]')
		let value = input.value

		if (input.name !== 'telefone') {
			if (input.value === '') return false
		}
	}

	return true
}

Form.prototype.handleSubmit = function (event) {
	event.preventDefault()
	let section = document.querySelector('#contato')
	let form = document.querySelector('form')
	let inputs = form.querySelectorAll('[name]')
	let params = new URLSearchParams()

	;[].forEach.call(inputs, function (el, i) {
		params.append(el.name, el.value)
	})

	if (this.validate(form, params)) {
		axios.post(form.action, params)
			.then(function (res) {
				form.reset()
				alert('Formulário enviado com sucesso!')
			})
			.catch(function (err) {
				alert('Erro ao enviar formulário. Tente novamente mais tarde')
			})
	} else {
		alert('Preencha os campos obrigatórios')
	}
}

Form.prototype.render = function (event) {
	console.log('log > Jornada > Form - render called!')

	let btnSubmit = document.querySelector('form .btn')

	this.Event.addTo(btnSubmit, 'click', 'handleSubmit')
}